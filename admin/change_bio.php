<?php

include_once "../utils/Message.php";
include_once "../utils/Bio.php";

$content = $_POST["content"];
$userId = $_POST["user-id"];

if(empty(trim($content))) {
    Message::error("Bio content cannot be empty");
    header("location: /admin/bio.php", 400);
}

if(empty(trim($userId))) {
    Message::error("Invalid request");
    header("location: /admin/bio.php", 400);
}

Bio::updateBio($content, $userId);

?>