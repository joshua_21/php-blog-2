<?php

include_once "../utils/auth-utils.php";
include_once "../utils/Message.php";

$adminLoggedIn = $loggedInAsAdmin;

if(!$adminLoggedIn) {
	Message::info("Log In To Continue");
	header("location: /admin/login.php?next=index");
}

// breadcrumbs
if(!isset($_SESSION)) {
	session_start();
}

$_SESSION["current"] = "home";

?>
<!DOCTYPE html>
<html>
<head>
	<title>Admin Menu</title>
	<?php include_once "../templates/css.html" ?>
</head>
<body>
<?php
include "breadcrumbs.php";
Message::show_message();
?>
<div class="container-fluid">
	<h2>
		<u>Admin Menu</u>
	</h2>
	<div class="border w-md-50 rounded">
		<p class="ml-2">
			<a href="/admin/user-list.php">Users</a>
		</p>
		<p class="ml-2">
			<a href="/admin/feedback.php">Feedback</a>
		</p>
		<p class="ml-2">
			<a href="/admin/subscribers.php">Subscribers</a>
		</p>
		<p class="ml-2">
			<a href="/admin/mail.php">Mail</a>
		</p>
		<p class="ml-2">
			<a href="/admin/contacts.php">Contacts</a>
		</p>	
		<p class="ml-2">
			<a href="/admin/locations.php">Locations</a>
		</p>	
		<p class="ml-2">
			<a href="/admin/bio.php">Bio</a>
		</p>
		<p class="ml-2">
			<a href="/admin/delete-duplicates.php">Delete Duplicate Subscriptions</a>
		</p>
		<p class="ml-2">
			<a href="/admin/newsletter-list.php">Newsletters</a>
		</p>
	</div>
</div>

</body>
</html>