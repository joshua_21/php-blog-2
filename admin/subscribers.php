<?php

include_once "../utils/auth-utils.php";
include_once "../utils/Message.php";
include_once "../utils/Subscriber.php";

$adminLoggedIn = $loggedInAsAdmin;

if(!$adminLoggedIn) {
	Message::info("Log In To Continue");
	header("location: /admin/login.php?next=user-list");
}

$pageNumber = empty($_GET["page"]) ? 1 : $_GET["page"];

$subscriptions = Subscriber::getSubscribersList($pageNumber);

// breadcrumbs
if(!isset($_SESSION)) {
	session_start();
}

$_SESSION["current"] = "subscribers";

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php include_once "../templates/css.html"; ?>
	<title>Subscribers list</title>
</head>
<body>
<?php include "breadcrumbs.php" ?>
<?php Message::show_message(); ?>

<table class="table table-striped table-hover">
<thead class="thead-dark">
	<td>Id</td>
	<td>name</td>
	<td>email</td>
	<td></td>
</thead>
<tbody>

<?php
foreach($subscriptions as $subscription) {
	$id = $subscription->id;
	$name = $subscription->name;
	$email = $subscription->email;
    echo '
        <tr>
            <td>'.$id.'</td>
            <td>'.$name.'</td>
            <td>'.$email.'</td>
            <td>
            <form class="form-inline" method="post" action="delete-subscriber.php">
                <input type="hidden" value="'.$id.'" name="subscription-id">
                <button class="btn btn-danger btn-sm" type="submit">delete</button>
            </form>
            </td>
        </tr>
        </div>
    ';
}
?>

</tbody>
</table>

<?php include "../templates/js.html"; ?>
</body>
</html>