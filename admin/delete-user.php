<?php

include_once "../utils/User.php";
include_once "../utils/Message.php";

$userId = $_POST["user-id"];

// send the user id and a boolean to indicate that the admin is the one deleting the account
$userDeleted = User::delete($userId, true);

// check the deletion status and the reponse message from the server on user deletion
$deleted = $userDeleted[0];
$message = $userDeleted[1];

if($deleted) {
	Message::success($message);
	header("location: /admin/user-list.php");
} else {
	Message::error($message);
	header("location: /admin/user-list.php");
}

?>