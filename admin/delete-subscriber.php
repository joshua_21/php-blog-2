<?php

include_once "../utils/Subscriber.php";
include_once "../utils/Message.php";

$id = $_POST["subscription-id"];

if(empty($id)) {
    Message::error("Invalid Request");
    header("location: /admin/subscribers.php");
}

Subscriber::delete($id);

Message::success("Subscriber deleted successfully");
header("location: /admin/subscribers.php");

?>