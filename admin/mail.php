<?php

include_once "../utils/auth-utils.php";
include_once "../utils/Message.php";

$adminLoggedIn = $loggedInAsAdmin;

if(!$adminLoggedIn) {
	Message::info("Log In To Continue");
	header("location: /admin/login.php?next=index");
}

// breadcrumbs
if(!isset($_SESSION)) {
	session_start();
}

$_SESSION["current"] = "mail";

$userId = $_COOKIE["userId"];

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php include_once "../templates/css.html" ?>
    <title>Send Weekly Newsletter</title>
</head>
<body>
<?php

include "breadcrumbs.php";
Message::show_message();

if(!isset($_SESSION)) {
    session_start();
}

// display 
$content = empty($_SESSION["tempContent"]) ? "" : $_SESSION["tempContent"]; 
$title = empty($_SESSION["tempTitle"]) ? "" : $_SESSION["tempTitle"];


function showTitle() {
    if(!empty($title)) {
        echo 'value = "'.$title.'"';
    }
}

function showContent() {
    if(!empty($content)) {
        echo 'value = "'.$content.'"';
    }
}
?>
<div class="container">
    <h3>
        <u>Send Weekly Newsletter</u>
    </h3>
    <div class="row">
        <form class="w-md-50" action="send-mail.php" method="post">
            <div class="form-group">
                <label for="title">Title</label>
                <input <?php showTitle(); ?> type="text" class="form-control" name="title">
            </div>
            <div class="form-group">
                <label for="content">Content</label>
                <textarea <?php showContent(); ?> class="form-control ckeditor" id="content" name="content"></textarea>
            </div>
            <input type="hidden" value="<?php echo $userId ?>" name="author">
            <input type="submit" value="Send">
        </form>
    </div>
</div>
<?php
unset($_SESSION["tempContent"]);
unset($_SESSION["tempTitle"]);
include "../templates/js.html"
?>
</body>
</html>