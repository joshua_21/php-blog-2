<?php

include_once "../utils/auth-utils.php";
include_once "../utils/Message.php";

$adminLoggedIn = $loggedInAsAdmin;

if(!$adminLoggedIn) {
	Message::info("Log In To Continue");
	header("location: /admin/login.php?next=user-list");
}

$pageNumber = empty($_GET["page"]) ? 1 : $_GET["page"];

$users = User::userList($pageNumber);

// breadcrumbs
if(!isset($_SESSION)) {
	session_start();
}

$_SESSION["previous"] = "home";
$_SESSION["current"] = "user-list";

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php include_once "../templates/css.html"; ?>
	<title></title>
</head>
<body>
<?php include "breadcrumbs.php" ?>
<?php Message::show_message(); ?>

<table class="table table-striped table-hover">
<thead class="thead-dark">
	<td>Id</td>
	<td>username</td>
	<td>email</td>
	<td>administrator</td>
	<td>active</td>
	<td></td>
</thead>
<tbody>

<?php
foreach($users as $user) {
	$id = $user->id;
	$userName = $user->username;
	$email = $user->email;
	$isAdmin = $user->is_admin ? "Y" : "N";
	$userId = $user->id;
	$isActive = $user->is_active;

	if($isActive == 0) {
		$isActive = "N";
	} else {
		$isActive = "Y";
	}

	$loggedInUserId = $_COOKIE["userId"];

	// dont display the current logged in user
	if($id != $loggedInUserId) {
		echo '
			<tr>
				<td>'.$id.'</td>
				<td>
					<a href="/admin/user-detail.php?id='.$userId.'">
					'.$userName.'
					</a>
				</td>
				<td>'.$email.'</td>
				<td>'.$isAdmin.'</td>
				<td>'.$isActive.'</td>
				<td>
					<button class="btn btn-danger btn-sm toggle-delete-popup" id="delete-user-'.$id.'">Delete</button>
				</td>
			</tr>
			<div id="delete-popup-'.$id.'" class="user-delete-popup border w-50">
				<p>Delete the user</p>
				<div class="d-flex">
					<form class="form-inline" method="post" action="delete-user.php">
						<input type="hidden" value="'.$id.'" name="user-id">
						<button class="btn btn-danger btn-sm mr-3" type="submit">Yes</button>
					</form>
					<button class="btn btn-success btn-sm close-delete-popup">No</button>
				</div>
			</div>
		';
	}
}
?>

</tbody>
</table>

<?php include "../templates/js.html"; ?>
</body>
</html>