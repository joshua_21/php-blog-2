<?php

include_once "../utils/Message.php";
include_once "../utils/Contacts.php";
include_once "../utils/auth-utils.php";
include_once "../utils/Locations.php";

$adminLoggedIn = $loggedInAsAdmin;

if(!$adminLoggedIn) {
	Message::info("Log In To Continue");
	header("location: /admin/login.php?next=contacts");
}

$contacts = Contact::getContacts();

$phone = $contacts->phone;
$phone2 = $contacts->phone_2;
$email = $contacts->email;
$mail = $contacts->mail;

// breadcrumbs
if(!isset($_SESSION)) {
	session_start();
}

$_SESSION["current"] = "contacts";

// locations
$locations = Location::getLocations();

$headOfficePresent = Location::HOPresent();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php include "../templates/css.html"; ?>
    <title>Contacts Page</title>
</head>
<body>
<?php

include "breadcrumbs.php";
Message::show_message();

?>
<!-- display contacts -->
<div class="container mt-5 mb-5">
    <button class="btn btn-outline-info" id="contacts-form-toggler">
        Update Contacts
    </button>
    <h2 class="mt-2">
        <u>Contacts</u>
    </h2>
    <p>
        Phone: <?php echo $phone; ?>
    </p>
    <p>
        Land Line: <?php echo $phone2; ?>
    </p>
    <p>
        Email: 
        <a href="mailto:<?php echo $email; ?>">
            <?php echo $email; ?>
        </a>
    </p>
    <p>
        Mail: <?php echo $mail; ?>
    </p>

    <!-- edit contacts form -->
    <form method="post" action="update-contacts.php" id="contacts-form" class="w-md-50">
        <fieldset>
            <legend><u>Update Contacts</u></legend>
            <div class="form-group">
                <label for="phone">Phone</label>
                <input value="<?php echo $phone; ?>" required type="text" name="phone" id="phone" class="form-control">
            </div>
            <div class="form-group">
                <label for="phone2">Land line</label>
                <input value="<?php echo $phone2; ?>" type="text" name="phone2" id="phone2" class="form-control">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input value="<?php echo $email; ?>" type="email" name="email" id="email" class="form-control">
            </div>
            <div class="form-group">
                <label for="mail">Mail</label>
                <input value="<?php echo $mail; ?>" required type="text" name="mail" id="mail" class="form-control">
            </div>
            <input type="submit" value="update contacts">
        </fieldset>
    </form>

    <div class="mt-3">
        <h2><u>Locations</u></h2>
        <?php
        foreach($locations as $location) {
            $city = $location->city;
            $openHours = $location->open_hours;
            $id = $location->id;
            $isHO = $location->is_head_office;
            $street = $location->street;

            echo '<div class="container mt-4 rounded border py-2">';
            if($isHO == "Y") {
                // for the head office
                echo '
                <div class="px-2">
                    <h3>
                        <u>Head Office</u>
                    </h3>
                    <p>City: '.$city.'</p>
                    <p>Open Hours: '.$openHours.'</p>
                    <p>Street: '.$street.'</p>
                </div>
                ';
            } else {
                // for the rest of the offices
                echo '
                    <p>City: '.$city.'</p>
                    <p>Open Hours: '.$openHours.'</p>
                    <p>Street: '.$street.'</p>
                ';
            }
            echo '</div>';
        }
        ?>
    </div>
</div>
<?php include "../templates/js.html" ?>
</body>
</html>