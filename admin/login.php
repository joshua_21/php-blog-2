<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php include_once "../templates/css.html"; ?>
    <title>Admin Login</title>
</head>
<body>

<?php

include_once "../utils/Message.php";
?>

<div class="container my-5">
    <?php
    // display alert 
    Message::show_message();
    ?>
    <form method="POST" action="login_admin.php" class="w-md-50 login-form">
        <legend><u>Admin Login Page</u></legend>
        <fieldset>
            <div class="form-group">
                <label for="username">Username</label>
                <input class="form-control" type="text" name="username" id="username">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input class="form-control" type="password" name="password" id="password">
            </div>
            <?php
            if(!empty($_GET["next"])) {
                echo '<input name="next" type="hidden" value="'.$_GET['next'].'">';
            }
            if(!empty($_GET["id"])) {
                echo '<input name="id" type="hidden" value="'.$_GET['id'].'">';
            }
            echo '<input type="submit" value="login">';
            ?>
        </fieldset>
        <br>
        <a href="sign_up.php">Don't have an account yet?</a>
    </form>
</div>

<?php
include_once "../templates/footer.php";
include_once "../templates/js.html";
?>
</body>
</html>