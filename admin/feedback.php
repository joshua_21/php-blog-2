<?php


include_once "../utils/auth-utils.php";
include_once "../utils/Message.php";
include_once "../utils/User.php";

// loggedInAsAdmin set in auth-utils
$adminLoggedIn = $loggedInAsAdmin;

if(!$adminLoggedIn) {
	Message::info("Log In To Continue");
	header("location: /admin/login.php?next=feedback");
}

$page = empty($_GET["page"]) ? 1 : $_GET["page"];

$feedbacks = User::getFeedback($page);

// breadcrumbs
if(!isset($_SESSION)) {
	session_start();
}

$_SESSION["previous"] = "home";
$_SESSION["current"] = "feedback-list";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php include "../templates/css.html"; ?>
    <title>Feedback Page</title>
</head>
<body>
<?php include "breadcrumbs.php" ?>

<?php

include_once "../utils/Message.php";

// display alert
Message::show_message();
?>

<table class="table table-striped table-hover">
<thead class="thead-dark">
	<td>Id</td>
	<td>type</td>
	<td>email</td>
	<td>Status</td>
</thead>
<tbody>

<?php
if(!empty($feedbacks)) {
	foreach($feedbacks as $feedback) {
		$id = $feedback->id;
		$email = $feedback->email;
		$type = $feedback->type;
		$viewed = $feedback->viewed;

		if($viewed == 1) {
			$status = "Viewed";
		} else {
			$status = "Not Viewed";
		}
			echo '
				<tr>
					<td>
						<a href="/admin/feedback-detail.php?id='.$id.'">
						'.$id.'
						</a>
					</td>
					<td>'.$type.'</td>
					<td>'.$email.'</td>
					<td>'.$status.'</td>
				</tr>
			';
	}
} else {
	echo "<span>No Feedback</span>";
}
?>

</tbody>
</table>

<?php
include "../templates/js.html";
include "../templates/footer.php";
?>

</body>
</html>