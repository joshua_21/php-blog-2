<?php

include_once "../utils/Message.php";
include_once "../utils/NewsLetter.php";
include_once "../utils/Subscriber.php";

if(!isset($_SESSION)) {
    session_start();
}

$content = $_POST["content"];
$title = $_POST["title"];
$authorId = $_POST["author"];

// temporarily store variables in session so that the user doesnt have to retype

$_SESSION["tempContent"] = $content;
$_SESSION["tempTitle"] = $title;

if(empty(trim($content)) || empty(trim($title)) || empty(trim($authorId))) {
    Message::error("All Fields Are Required");
    header("location: /admin/mail.php");
}

// send mail and save to db
$mailRecipients = Subscriber::getSubscriberEmails();

$mailHeader = "MIME-Version: 1.0"."\r\n";
$mailHeader .= "Content-type: text/html;charset=UTF-8"."\r\n";
$mailHeader .= "From: <usersadmin@scammersinc>"."\r\n";
$mailHeader .= "Cc: sitemanager@scammersinc.com"."\r\n";


$message = '
<!DOCTYPE html>
<html>
<head></head>
<body>
'.$content.'
<p>
<a href="#">Unsubscribe</a> | <a href="#">Help</a>
</p>
</body>
</html>
';

// send the mail to the subscribers
foreach($mailRecipients as $recipient) {
    mail($recipient->email, $title, $message, $mailHeader);
}

NewsLetter::save($title, $content, $authorId);

Message::success("Mail Sent Successfully");
header("location: /admin");

?>