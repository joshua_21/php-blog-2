<?php

include_once "../utils/DBConnector.php";
include_once "../utils/Subscriber.php";
include_once "../utils/Message.php";

Subscriber::deleteDuplicates();

Message::success("The duplicate entries were cleared successfully");
header("location: /admin/index.php");

?>