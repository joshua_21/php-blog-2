<?php

include_once "../utils/User.php";

$userId = $_POST["user-id"];
$action = $_POST["action"];

if(!empty($userId)) {
	if($action == "add-admin") {
		User::addSuperUser($userId);
		Message::success("Super User Status Added Successfully");
		header("location: /admin/user-detail.php?id=$userId");
	} elseif($action == "remove-admin") {
		User::removeSuperUser($userId);
		Message::success("Super User Status Removed Successfully");
		header("location: /admin/user-detail.php?id=$userId");
	} else {
		Message::error("Invalid Action");
		header("lcoation: /admin/user-detail.php?id=$userId");
	}
}

?>