<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php

    include "../templates/css.html";
    include_once "../utils/User.php";
    include_once "../utils/auth-utils.php";
    include_once "../utils/Message.php";

    // get user details
    $userId = empty($_GET["id"]) ? null : $_GET["id"];

    $adminLoggedIn = $loggedInAsAdmin;

    if(!$adminLoggedIn) {
        Message::info("Log In To Continue");
        header("location: /admin/login.php?next=user-detail&id=$userId");
    }

    // ensure the user id is not empty
    if(empty($userId)) {
        Message::error("User Id Cannot Be Empty");
        header("location: /admin/user-list.php", 400);
    }

    // get user details
    $userDetails = User::getDetails($userId);

    // parse the user details
    $firstName = $userDetails["firstName"];
    $lastName = $userDetails["lastName"];
    $userName = $userDetails["userName"];
    $email = $userDetails["email"];
    $dateOfBirth = $userDetails["dateOfBirth"];
    $imageUrl = $userDetails["imageUrl"];
    $phoneNumber = $userDetails["phoneNumber"];
    $bio = $userDetails["bio"];

    // breadcrumbs
    if(!isset($_SESSION)) {
        session_start();
    }

    $_SESSION["previous"] = "user-list";
    $_SESSION["current"] = "user-detail";
    
    ?>
    <title><?php echo $userName; ?>'s profile</title>
</head>
<body>
    <?php include "breadcrumbs.php" ?>
    <?php
    Message::show_message();
    ?>
    <div class="container mx-1 mx-sm-3 mx-md-5">
        <div class="">
            <h5 class="mr-auto"><?php echo $userName; ?>'s profile</h5>
            <br>
            <div class="row">
                <form class="form-inline mb-2 mb-sm-0 mt-2" method="post" action="delete-user.php">
                    <input type="hidden" value="<?php echo $userId; ?>" name="user-id">
                    <input type="submit" value="Delete User" class="btn btn-sm btn-outline-danger">
                </form>
                <span class="ml-3"></span>
                <?php
                // echo $userId;die();
                if(User::isAdmin($userId)) {
                    // remove the superuser status
                    echo '
                    <form class="form-inline mt-2" method="post" action="edit-user.php">
                        <input type="hidden" value="'.$userId.'" name="user-id">
                        <input type="hidden" value="remove-admin" name="action">
                        <input type="submit" value="Remove Admin" class="btn btn-sm btn-outline-success">
                    </form>
                    ';
                } else {
                    // make the user a superuser
                    echo '
                    <form class="form-inline mt-2" method="post" action="edit-user.php">
                        <input type="hidden" value="'.$userId.'" name="user-id">
                        <input type="hidden" value="add-admin" name="action">
                        <input type="submit" value="Make Admin" class="btn btn-sm btn-outline-success">
                    </form>
                    ';
                }
                ?>
            </div>
        </div>
        <div class="row mt-3">
        <!-- image banner -->
            <div class="mr-auto">
                <img src="<?php echo $imageUrl; ?>" height="100">
            </div>
            <!-- contacts banner -->
            <div class="ml-sm-4 mr-3">
                <p>First Name: <?php echo $firstName; ?><p>
                <p>Last Name: <?php echo $lastName; ?></p>
                <p>Username: <?php echo $userName; ?></p>
                <p>Email: <?php echo $email; ?></p>
                <p>Phone: <?php echo empty($phoneNumber) ? "Not Provided" : $phoneNumber; ?></p>
                <p>Date Of Birth: <?php echo $dateOfBirth; ?></p>
            </div>
        </div>
        <div class="row">
            <h2 class="col-12">Bio</h2>
            <p><?php echo empty($bio) ? "Not Provided" : $bio; ?></p>
        </div>
    </div>

    <?php include "../templates/footer.php"; ?>
    <?php include "../templates/js.html"; ?>
</body>
</html>