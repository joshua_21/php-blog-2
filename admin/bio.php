<?php

include_once "../utils/auth-utils.php";
include_once "../utils/Message.php";
include_once "../utils/Bio.php";

$adminLoggedIn = $loggedInAsAdmin;

if(!$adminLoggedIn) {
	Message::info("Log In To Continue");
	header("location: /admin/login.php?next=bio");
}

// breadcrumbs
if(!isset($_SESSION)) {
	session_start();
}

$_SESSION["current"] = "bio";

$bioExists = Bio::bioExists();
$userId = $_COOKIE["userId"];

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php include_once "../templates/css.html" ?>
    <title>Bio Page</title>
</head>
<body>
<?php
include "breadcrumbs.php";
Message::show_message();

if($bioExists) {
    $bio = Bio::getBio();
    echo '
    <div class="container">
    <p><button id="btn-toggle-bio-update-form" class="btn btn-outline-primary">Update Bio</button></p>
    <form method="post" id="form-update-bio" action="change_bio.php">
        <div class="form-group">
            <label for="content" class="sr-only">
                content
            </label>
            <textarea class="ckeditor" id="content" name="content">'.$bio->content.'</textarea>
        </div>
        <input type="hidden" name="user-id" value="'.$userId.'">
        <input type="submit" value="submit">
    </form>
    <p>Last updated by '.$bio->updatedBy.' on '.$bio->lastUpdated.'</p>
    <p>'.$bio->content.'</p>
    </div>
    ';
} else {
    echo '
    <div class="container">
    <div class="row">
    <p class="text text-info lead col-12 ml-0">There is currently no bio</p>
    <form method="post" action="change_bio.php">
        <div class="form-group">
            <label for="content">
                Enter Bio Below
            </label>
            <textarea rows="10" class="ckeditor form-control" id="content" name="content"></textarea>
        </div>
        <input type="hidden" name="user-id" value="'.$userId.'">
        <input type="submit" value="submit">
    </form>
    </div>
    </div>
    ';
}
?>
<?php include "../templates/js.html" ?>
</body>
</html>