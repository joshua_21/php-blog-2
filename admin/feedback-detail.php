<?php

include_once "../utils/auth-utils.php";
include_once "../utils/Message.php";
include_once "../utils/User.php";

$adminLoggedIn = $loggedInAsAdmin;

$id = empty($_GET["id"])? null: $_GET["id"];

if(empty($id)) {
	Message::error("Feedback Id Cannot Be Empty");
	header("location: /admin/feedback.php");
}

if(!$adminLoggedIn) {
	Message::info("Log In To Continue");
	header("location: /admin/login.php?next=feedback-detail&id=$id");
}

$userId = $_COOKIE["userId"];

User::viewFeedback($id, $userId);

$feedback = User::feedBackDetails($id);

function formatDate($dbDate) {
	$sqlDate = DateTime::createFromFormat("Y-m-d H:i:s", $dbDate);
	$formattedDate = $sqlDate->format("d/m/Y");
	return $formattedDate;
}

// feedback details
$name = $feedback->name;
$email = $feedback->email;
$title = $feedback->title;
$type = $feedback->type;
$content = $feedback->content;
$userViewedId = $feedback->viewed_by;
$dateViewed = formatDate($feedback->date_viewed);
$datePosted = formatDate($feedback->date_posted);

// get the username
$userViewed = User::getDetails($userViewedId)["userName"];

// breadcrumbs
if(!isset($_SESSION)) {
	session_start();
}

$_SESSION["previous"] = "feedback-list";
$_SESSION["current"] = "feedback-detail";

?>
<!DOCTYPE html>
<html>
<head>
	<title>Feedback Details</title>
	<?php include_once "../templates/css.html" ?>
</head>
<body>
	<?php include "breadcrumbs.php"; ?>
	<div class="container-fluid">
		<p><u>Title</u>: <?php echo $title; ?></p>
		<p><u>Type</u>: <?php echo $type;  ?></p>
		<p><u>User</u>: <?php echo $name; ?></p>
		<p>
			<u>Email</u>: <a href="mailto:<?php echo $email; ?>">
			<?php echo $email; ?></a>
		</p>
		<p><u>Date</u>: <?php echo $datePosted; ?></p>
		<p>
			<u>Viewed By</u>: 
			<a href="/admin/user-detail.php?id=<?php echo $userViewedId ?>">
				<?php echo $userViewed; ?>
			</a> On <?php echo $dateViewed; ?>
		</p>
		<h2>
			<u>Content</u>
		</h2>
		<p><?php echo $content; ?></p>
	</div>
</body>
</html>