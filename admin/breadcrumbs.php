<?php

if(!isset($_SESSION)) {
	session_start();
}

$previousPage = empty($_SESSION["previous"]) ? "" : $_SESSION["previous"];
$currentPage = $_SESSION["current"];

echo "<div class='breadcrumb lead'>";
// on home page
if($currentPage == "home") {
	echo "<span>/ <a href='/admin'>root</a></span>";
} elseif($currentPage == "locations") {
	// on user list page
	echo "
	<span>
	/ <a href='/admin'>root</a> /
	<a href='/admin/locations.php'>locations</a>
	</span>
	";
} elseif($currentPage == "contacts") {
	// on user list page
	echo "
	<span>
	/ <a href='/admin'>root</a> /
	<a href='/admin/contacts.php'>contacts</a>
	</span>
	";
} elseif($currentPage == "user-list") {
	// on user list page
	echo "
	<span>
	/ <a href='/admin'>root</a> /
	<a href='/admin/user-list.php'>user-list</a>
	</span>
	";
} elseif($currentPage == "newsletter-list") {
	// on feedback list page
	echo "
	<span>
	/ <a href='/admin'>root</a> /
	<a href='/admin/newsletter-list.php'>newsletters</a>
	</span>
	";
} elseif($currentPage == "mail") {
	// on feedback list page
	echo "
	<span>
	/ <a href='/admin'>root</a> /
	<a href='/admin/mail.php'>mail</a>
	</span>
	";
} elseif($currentPage == "subscribers") {
	// on feedback list page
	echo "
	<span>
	/ <a href='/admin'>root</a> /
	<a href='/admin/subscribers.php'>subscribers</a>
	</span>
	";
} elseif($currentPage == "bio") {
	// on feedback list page
	echo "
	<span>
	/ <a href='/admin'>root</a> /
	<a href='/admin/bio.php'>bio</a>
	</span>
	";
} elseif($currentPage == "feedback-list") {
	// on feedback list page
	echo "
	<span>
	/ <a href='/admin'>root</a> /
	<a href='/admin/feedback.php'>feedback-list</a>
	</span>
	";
} elseif($previousPage == "user-list") {
	// on user detail page
	$userId = $_GET["id"];
	echo "
	<span>
	/ <a href='/admin'>root</a> /
	<a href='/admin/user-list.php'>user-list</a> /
	<a href='/admin/user-detail.php?id=$userId'>$userId</a>
	</span>
	";
} elseif($previousPage == "feedback-list") {
	// on feedback detail page
	$feedbackId = $_GET["id"];
	echo "
	<span>
	/ <a href='/admin'>root</a> /
	<a href='/admin/feedback.php'>feedback-list</a> /
	<a href='/admin/feedback-detail.php?id=$feedbackId'>$feedbackId</a>
	</span>
	";
}
echo "<a href='/users/logout.php?next=admin-login' class='ml-auto'>logout</a>";
echo "<a href='/' class='ml-5'>home</a>";
echo "</div>";

?>