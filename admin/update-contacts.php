<?php

include_once "../utils/Contacts.php";
include_once "../utils/Message.php";

foreach($_POST as $item) {
    $theItem = $item;
    if(empty($theItem)) {
        Message::error("All Fields Are Required");
        header("location: /admin/contacts.php");
    }
}

$phone = $_POST["phone"];
$phone2 = $_POST["phone2"];
$email = $_POST["email"];
$mail = $_POST["mail"];

Contact::update($phone, $phone2, $email, $mail);

Message::success("Contacts Updated Successfully");

header("location: /admin/contacts.php");

?>