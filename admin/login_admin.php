<?php
session_start();

include_once "../utils/User.php";
include_once "../utils/Message.php";

// login the user
if(!empty(trim($_POST["username"])) && !empty(trim($_POST["password"]))) {
    $username = $_POST["username"];
    $password = $_POST["password"];
    
    User::login($username, $password);

    // the user id is temporarily stored with the session before it is saved in the browser
    // the cookie will be saved in the browser after this script exits hence we cannot use $_COOKIE here
    $userId = $_SESSION["userId"];

    // ********* hard to comprehend ************
    $_SESSION["adminLogin"] = true;
    $_SESSION["next"] = empty($_POST["next"]) ? $_GET["next"] : $_POST["next"];
    $_SESSION["postId"] = empty($_POST["id"]) ? $_GET["id"] : $_POST["id"];
    // ******** end of hard to comprehend ***********

    if(User::isAdmin($userId)) {
        // clear the temporary user id
        unset($_SESSION["userId"]);

        Message::success("Admin Successfully logged in");
        // get next and id for redirection
        $next = empty($_POST["next"]) ? $_GET["next"] : $_POST["next"];
        $id = $_POST["id"];

        // ********* hard to comprehend ************
        if(empty($next)) {
            $next = $_SESSION["next"];
        }

        if(empty($id)) {
            $id = $_SESSION["id"];
        }

        // unset session variables after the admin has logged in
        unset($_SESSION["next"]);
        unset($_SESSION["id"]);
        unset($_SESSION["adminLogin"]);
        // ******** end of hard to comprehend ***********

        if(!empty($id)) {
            header("location: /admin/$next.php&id=$id");
        } elseif(!empty($next)) {
            header("location: /admin/$next.php");
        } else {
            header("location: /admin");
        }
    } else {
        // clear the temporary user id
        unset($_SESSION["userId"]);

        // the user is not an administrator
        Message::error("You Need To Log In As An Administrator");
        setcookie("userId", "", time()-3600, "/");

        if(!empty($next)) {
            header("location: /admin/login.php?next=$next", 400);
        } else {
            header("location: /admin");
        }
        
    }
} else {
    Message::error("Both The Username And Password Are Required");
    header("location: /users/login.php", 400);
}

?>