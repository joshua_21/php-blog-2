<?php

include_once "../utils/Message.php";
include_once "../utils/Locations.php";

function paramsValid($params) {
    foreach($params as $param) {
        if(empty(trim($param))) {
            return false;
        }
    }
    return true;
}

$transactionType = $_POST["transaction-type"];
$city = $_POST["city"];
$openHours = $_POST["open-hours"];
$street = $_POST["street"];
$id = $_POST["location-id"];

if(empty($transactionType)) {
    Message::error("invalid request");
    header("location: /admin/locations.php", 400);
}

if($transactionType == "update") {
    $cannotBeEmpty = [$city, $openHours, $street, $id];

    if(paramsValid($cannotBeEmpty)) {
        Location::updateLocation($id, $city, $openHours, $street);
        Message::success("location was updated successfully");
        header("location: /admin/locations.php", 200);
    } else {
        Message::error("all fields are required");
        header("location: /admin/locations.php", 400);
    }
} elseif($transactionType == "create") {
    $cannotBeEmpty = [$city, $openHours, $street];

    if(paramsValid($cannotBeEmpty)) {
        Location::createLocation($city, $openHours, $street);
        Message::success("location was created successfully");
        header("location: /admin/locations.php", 201);
    } else {
        Message::error("all fields are required");
        header("location: /admin/locations.php", 400);
    }
} elseif($transactionType == "delete") {
    $cannotBeEmpty = [$id];

    if(paramsValid($cannotBeEmpty)) {
        Location::deleteLocation($id);
        Message::success("location was deleted successfully");
        header("location: /admin/locations.php", 200);
    } else {
        Message::error("all fields are required");
        header("location: /admin/locations.php", 400);
    }
} elseif($transactionType == "create-ho") {
    $cannotBeEmpty = [$city, $openHours, $street];

    if(paramsValid($cannotBeEmpty)) {
        Location::createHO($city, $openHours, $street);
        Message::success("head office created successfully");
        header("location: /admin/locations.php", 200);
    } else {
        Message::error("all fields are required");
        header("location: /admin/locations.php", 400);
    }
}

?>