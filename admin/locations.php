<?php

include_once "../utils/Locations.php";
include_once "../utils/auth-utils.php";
include_once "../utils/Message.php";
include_once "../utils/Locations.php";

$adminLoggedIn = $loggedInAsAdmin;

if(!$adminLoggedIn) {
	Message::info("Log In To Continue");
	header("location: /admin/login.php?next=locations");
}

// breadcrumbs
if(!isset($_SESSION)) {
	session_start();
}

$_SESSION["current"] = "locations";

$locations = Location::getLocations();

$headOfficePresent = Location::HOPresent();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php include "../templates/css.html" ?>
    <title>Locations</title>
</head>
<body class="pb-5">
<?php
    // display alert 
    Message::show_message();
    ?>
<?php 
include "breadcrumbs.php";
if(!$headOfficePresent) {
    echo '
    <div class="container-fluid">
        <p class="text text-danger">You do not have a head office</p>
        <div class="row">
            <button id="btn-toggle-ho-form" class="w-100 rounded-0 btn btn-info">Create Head Office</button>
        </div>
    </div>
    ';
}
?>
<div class="container">
    <div class="row mt-1">
    <!-- HO form -->
    <form id="form-create-head-office" class="col-12 mb-4" method="post" action="change_location.php">
            <fieldset>
                <legend><u>Head Office Form</u></legend>
                <input type="hidden" name="transaction-type" value="create-ho">
                <div class="form-group">
                    <label for="city">City</label>
                    <input placeholder="city where it's located" required type="text" name="city" id="city" class="form-control">
                </div>
                <div class="form-group">
                    <label for="open-hours">Open Hours</label>
                    <br>
                    <input placeholder="hh:mm am/pm - hh:mm am/pm" required type="text" name="open-hours" id="open-hours" class="form-control">
                </div>
                <div class="form-group">
                    <label for="street">Street Name</label>
                    <br>
                    <input placeholder="number streetname" required type="text" name="street" id="street" class="form-control">
                </div>
                <input type="submit" value="add">
            </fieldset>
        </form>
        <button class="btn btn-outline-primary" id="btn-add-location">Add Location</button>
        <form id="form-add-location" class="col-12" method="post" action="change_location.php">
            <fieldset>
                <legend><u>New Location</u></legend>
                <input type="hidden" name="transaction-type" value="create">
                <div class="form-group">
                    <label for="city">City</label>
                    <br>
                    <input placeholder="city where it's located" required type="text" name="city" id="city" class="form-control-sm">
                </div>
                <div class="form-group">
                    <label for="open-hours">Open Hours</label>
                    <br>
                    <input placeholder="hh:mm am/pm - hh:mm am/pm" required type="text" name="open-hours" id="open-hours" class="form-control-sm">
                </div>
                <div class="form-group">
                    <label for="street">Street Name</label>
                    <br>
                    <input placeholder="number streetname" required type="text" name="street" id="street" class="form-control-sm">
                </div>
                <input type="submit" value="add">
            </fieldset>
        </form>
    </div>
</div>
<p class="mt-4"></p>
<?php
foreach($locations as $location) {
    $city = $location->city;
    $openHours = $location->open_hours;
    $id = $location->id;
    $isHO = $location->is_head_office;
    $street = $location->street;

    echo '<div class="container mt-4 rounded border py-2">';
    if($isHO == "Y") {
        // for the head office
        echo '
        <div class="px-2">
            <h3>
                <u>Head Office</u>
            </h3>
            <div class="d-flex">
                <button id="btn-update-location-'.$id.'" class="btn btn-sm btn-outline-info mr-2 btn-toggle-update-form">Update</button>
                <button id="btn-delete-location-'.$id.'" class="btn-toggle-delete-form btn btn-sm btn-outline-danger">Delete</button>
            </div>
            <!-- delete location popup -->
            <div id="form-delete-location-'.$id.'" class="delete-location-popup bg-warning w-md-50 rounded p-2">
                <p>Are you sure you want to delete the location?</p>
                <div class="d-flex">
                    <form class="form-inline mr-2" method="post" action="change_location.php">
                        <input type="hidden" name="transaction-type" value="delete">
                        <input type="hidden" name="location-id" value="'.$id.'">
                        <input type="submit" value="yes" class="btn btn-danger btn-sm">
                    </form>
                    <button class="btn btn-success btn-sm minimise-delete-form" id="form-'.$id.'">no</button>
                </div>
            </div>

            <p>City: '.$city.'</p>
            <p>Open Hours: '.$openHours.'</p>
            <p>Street: '.$street.'</p>
        </div>
        <form action="change_location.php" class="form-update-location" id="form-update-location-'.$id.'" method="post">
            <fieldset>
                <legend><u>update location</u></legend>
                <input type="hidden" name="transaction-type" value="update">
                <div class="form-group">
                    <label for="city">City</label>
                    <input value="'.$city.'" required type="text" name="city" id="city" class="form-control">
                </div>
                <div class="form-group">
                    <label for="open-hours">Open Hours</label>
                    <input value="'.$openHours.'" required type="text" name="open-hours" id="open-hours" class="form-control">
                </div>
                <div class="form-group">
                    <label for="street">Street Name</label>
                    <input value="'.$street.'" required type="text" name="street" id="street" class="form-control">
                </div>
                <input type="hidden" value="'.$id.'" name="location-id">
                <input type="submit" value="update">
            </fieldset>
        </form>
        ';
    } else {
        // for the rest of the offices
        echo '
            <div class="d-flex">
                <button id="btn-update-location-'.$id.'" class="btn btn-sm btn-outline-info mr-2 btn-toggle-update-form">Update</button>
                <button id="btn-delete-location-'.$id.'" class="btn btn-sm btn-outline-danger btn-toggle-delete-form">Delete</button>
            </div>
            <!-- delete location popup -->
            <div id="form-delete-location-'.$id.'" class="delete-location-popup bg-warning w-md-50 rounded p-2">
                <p>Are you sure you want to delete the location?</p>
                <div class="d-flex">
                    <form class="form-inline mr-2" method="post" action="change_location.php">
                        <input type="hidden" name="transaction-type" value="delete">
                        <input type="hidden" name="location-id" value="'.$id.'">
                        <input type="submit" value="yes" class="btn btn-danger btn-sm">
                    </form>
                    <button class="btn btn-success btn-sm minimise-delete-form" id="form-'.$id.'">no</button>
                </div>
            </div>
            <p>City: '.$city.'</p>
            <p>Open Hours: '.$openHours.'</p>
            <p>Street: '.$street.'</p>
            <form action="change_location.php" class="form-update-location" id="form-update-location-'.$id.'" method="post">
                <fieldset>
                    <legend><u>update location</u></legend>
                    <input type="hidden" name="transaction-type" value="update">
                    <div class="form-group">
                        <label for="city">City</label>
                        <input value="'.$city.'" required type="text" name="city" id="city" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="open-hours">Open Hours</label>
                        <input value="'.$openHours.'" required type="text" name="open-hours" id="open-hours" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="street">Street Name</label>
                        <input value="'.$street.'" required type="text" name="street" id="street" class="form-control">
                    </div>
                    <input type="hidden" value="'.$id.'" name="location-id">
                    <input type="submit" value="update">
                </fieldset>
            </form>
        ';
    }
    echo '</div>';
}

?>

<?php include "../templates/js.html"; ?>
</body>
</html>