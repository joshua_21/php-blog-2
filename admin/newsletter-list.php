<?php

include_once "../utils/auth-utils.php";
include_once "../utils/Message.php";
include_once "../utils/NewsLetter.php";

$adminLoggedIn = $loggedInAsAdmin;

if(!$adminLoggedIn) {
	Message::info("Log In To Continue");
	header("location: /admin/login.php?next=newsletter-list");
}

$pageNumber = empty($_GET["page"]) ? 1 : $_GET["page"];

$newsletters = NewsLetter::newsLetterList($pageNumber);

// breadcrumbs
if(!isset($_SESSION)) {
	session_start();
}

$_SESSION["current"] = "newsletter-list";

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php include_once "../templates/css.html"; ?>
	<title>Newsletters</title>
</head>
<body>
<?php include "breadcrumbs.php" ?>
<?php Message::show_message(); ?>

<table class="table table-striped table-hover table-bordered">
<thead class="thead-dark">
	<td>title</td>
	<td>content</td>
	<td>author</td>
	<td>date sent</td>
</thead>
<tbody>

<?php
foreach($newsletters as $newsletter) {
	$title = $newsletter->title;
	$content = $newsletter->content;
    $author = $newsletter->username;
    $dateSent = $newsletter->date_sent;
    echo '
        <tr>
            <td>'.$title.'</td>
            <td>'.$content.'</td>
            <td>'.$author.'</td>
            <td>'.$dateSent.'</td>
        </tr>
        </div>
    ';
}
?>

</tbody>
</table>

<?php include "../templates/js.html"; ?>
</body>
</html>