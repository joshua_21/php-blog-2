<?php

include_once "../utils/Message.php";
include_once "../utils/User.php";

$userId = $_POST["user-id"];

$userDeleted = User::delete($userId)[0];
$message = User::delete($userId)[1];

if($userDeleted) {
	Message::success($message);
	header("location: /", 200);
} else {
	Message::error($message);
	header("location: /users/update_profile.php");
}

?>