<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php

    include "../templates/css.html";
    include_once "../utils/User.php";
    include_once "../utils/Message.php";

    if(!isset($_SESSION)) {
        session_start();
    }

    if(!$_SESSION["isAuthenticated"] || empty($_COOKIE["userId"])) {
        Message::info("Log In First To Continue");
    }

    // get user details
    $userId = $_COOKIE["userId"];

    // get user details
    $userDetails = User::getDetails($userId);

    // parse the user details
    $firstName = $userDetails["firstName"];
    $lastName = $userDetails["lastName"];
    $userName = $userDetails["userName"];
    $email = $userDetails["email"];
    $dateOfBirth = $userDetails["dateOfBirth"];
    $imageUrl = $userDetails["imageUrl"];
    $phoneNumber = $userDetails["phoneNumber"];
    $bio = $userDetails["bio"];
    
    ?>
    <title><?php echo $userName; ?>'s profile</title>
</head>
<body>
    <?php include_once "../templates/header.php" ?>
    <div class="container mx-1 mx-sm-3 mx-md-5">
        <div class="">
            <h5 class="mr-auto"><?php echo $userName; ?>'s profile</h5>
            <br>
            <a href="/users/update_profile.php" class="btn btn-sm btn-outline-info mr-5 mb-4">Update Profile</a>
        </div>
        <div class="row">
        <!-- image banner -->
            <div class="mr-auto">
                <img src="<?php echo $imageUrl; ?>" height="100">
            </div>
            <!-- contacts banner -->
            <div class="ml-sm-4 mr-3">
                <p>First Name: <?php echo $firstName; ?><p>
                <p>Last Name: <?php echo $lastName; ?></p>
                <p>Username: <?php echo $userName; ?></p>
                <p>Email: <?php echo $email; ?></p>
                <p>Phone: <?php echo empty($phoneNumber) ? "Not Provided" : $phoneNumber; ?></p>
                <p>Date Of Birth: <?php echo $dateOfBirth; ?></p>
            </div>
        </div>
        <div class="row">
            <h2 class="col-12">Bio</h2>
            <p><?php echo empty($bio) ? "Not Provided" : $bio; ?></p>
        </div>
    </div>

    <?php include "../templates/footer.php"; ?>
    <?php include "../templates/js.html"; ?>
</body>
</html>