<?php
session_start();

include_once "../utils/User.php";
include_once "../utils/Message.php";

// login the user
if(!empty(trim($_POST["username"])) && !empty(trim($_POST["password"]))) {
    $username = $_POST["username"];
    $password = $_POST["password"];

    // check whether the user was to be redirected after logging in
    $next = empty($_POST["next"]) ? "" : $_POST["next"];
    $postId = empty($_POST["id"]) ? "" : $_POST["id"];

    // the post id is not in the post parameters, check in the get
    if(empty($postId)) {
        $postId = empty($_GET["id"]) ? "" : $_GET["id"];
    }

    $_SESSION["userId"] = $userId;
    $userId = User::login($username, $password, $next);

    // check whether user is supposed to be redirected to a previous page
    if($next == "update_profile") {
        header("location: /users/update_profile.php");
    } elseif($next == "comment") {
        header("location: /posts/post-detail.php?id=$postId");
    } elseif($next == "user-list") {
        header("location: /admin/user-list.php");
    } else {
        header("location: /", 200);
    }

} else {
    Message::error("Both The Username And Password Are Required");
    header("location: /users/login.php", 400);
}

?>