<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php
    // start the session so as to check whether the user is authenticated
    if(!isset($_SESSION)) {
        session_start();
    }

    include_once "../utils/Message.php";

    if(!isset($_SESSION["isAuthenticated"]) || $_SESSION["isAuthenticated"] == false) {
        Message::info("Log in first to continue");
        header("location: /users/login.php?next=update_profile");
    }
    include_once "../templates/css.html";
    ?>
    <title>Update Profile</title>
</head>
<body>
<?php

include_once "../templates/header.php";
include_once "../utils/Message.php";
include_once "../utils/User.php";

// display alert
Message::show_message();

$userId = $_COOKIE["userId"];

$user = User::getDetails($userId);

$userName = $user["userName"];
$email = $user["email"];
$firstName = $user["firstName"];
$lastName = $user["lastName"];
$DOB = $user["dateOfBirth"];
$imageUrl = $user["imageUrl"];
$phoneNumber = $user["phoneNumber"];
$bio = $user["bio"];

// split the image url to get the full image name
$substrings = explode("/", $imageUrl);
$image = end($substrings);

?>
    <div class="container">
        <form class="ml-5 form-inline" method="POST" action="delete_user.php">
            <input type="hidden" value="<?php echo $userId; ?>" name="user-id">
            <input class="btn btn-sm btn-outline-danger" type="submit" value="Delete Account">
        </form>
    </div>
    <form action="update.php" method="POST" enctype="multipart/form-data" class="container-fluid col-sm-8 col-md-6 col-12">
        <fieldset>
            <legend class="d-flex">
                <u>Update Profile</u>
            </legend>
            <?php //profile pic to be inserted soon ?>
            <div class="form-group mt-3">
                <label for="first-name">First name</label>
                <input value="<?php echo $firstName; ?>" name="first-name" class="form-control" type="text" id="first-name">
            </div>
            <div class="form-group">
                <label for="last-name">Last name</label>
                <input value="<?php echo $lastName; ?>" name="last-name" class="form-control" type="text" id="last-name">
            </div>
            <div class="form-group">
                <label for="username">Username</label>
                <input value="<?php echo $userName; ?>" name="username" class="form-control" type="text" id="username">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input value="<?php echo $email; ?>" name="email" class="form-control" type="email" id="email">
            </div>
            <div class="form-group">
                <label for="phone">Phone Number</label>
                <input value="<?php echo $phoneNumber ?>" name="phone" class="form-control" type="text" id="phone">
            </div>
            <div class="form-group">
                <label for="date-of-birth">Date of birth</label>
                <input value="<?php echo $DOB; ?>" name="date-of-birth" class="form-control" type="date" id="date-of-birth">
            </div>
            <div class="form-group">
                <label for="bio">Bio</label>
                <textarea name="bio" class="form-control ckeditor" id="bio">
                <?php echo(empty($bio) ? "" : $bio); ?>
                </textarea>
            </div>
            <input type="submit" value="Update Profile">
        </fieldset>
    </form>
    
    <?php 
        include "../templates/footer.php";
        include "../templates/js.html";
    ?>
</body>
</html>