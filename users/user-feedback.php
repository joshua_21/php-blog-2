<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php include "../templates/css.html"; ?>
    <title>Complain Or Suggest</title>
</head>
<body>
<?php
include "../templates/header.php";
?>

<div class="container justify-content-center d-flex">
	<form method="post" class="w-md-50" action="create-feedback.php">
		<fieldset>
			<legend>
				<u>Complain Or Suggest</u>
			</legend>
			<div class="form-group">
				<label for="name">Name</label>
				<input class="form-control" id="name" name="name" type="text">
			</div>
			<div class="form-group">
				<label for="email">Email</label>
				<input class="form-control" id="email" name="email" type="email">
			</div>
			<div class="form-group">
				<label for="title">Title</label>
				<input class="form-control" id="title" name="title" type="text">
			</div>
			<div class="form-group">
				<label for="content">Content</label>
				<textarea class="form-control" rows="10" id="content" name="content"></textarea>
			</div>
			<div class="form-group">
				<label for="feedback">Feedback</label>
				<input checked type="radio" name="type" value="feedback" id="feedback">
				<br>
				<label for="suggestion">Suggestion</label>
				<input type="radio" name="type" value="suggestion" id="suggestion">
				<br>
				<label for="complaint">Complaint</label>
				<input type="radio" name="type" value="complaint" id="complaint">
			</div>
			<input type="submit" value="Send Feedback">
		</fieldset>
	</form>
</div>

<?php
include "../templates/footer.php";
include "../templates/js.html";
?>
</body>
</html>