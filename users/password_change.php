<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php include_once "templates/css.html"; ?>
    <title>Change Password</title>
</head>
<body>

<?php include_once "templates/header.php"; ?>

<div class="container my-5">
    <form action="POST" class="w-75 login-form">
        <legend><u>Password Change Page</u></legend>
        <fieldset>
            <div class="form-group">
                <label for="username">Username</label>
                <input class="form-control" type="text" name="username" id="username">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input class="form-control" type="password" name="password" id="password">
            </div>
            <input type="submit" value="login">
        </fieldset>
    </form>
</div>

<?php
include_once "templates/footer.php";
include_once "templates/js.html";
?>
</body>
</html>