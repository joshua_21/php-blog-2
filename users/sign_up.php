<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php include "../templates/css.html"; ?>
    <title>Create New Account</title>
</head>
<body>
    <?php
        include "../templates/header.php";
        include_once "../utils/Message.php";

        // display alert
        Message::show_message();
    ?>

    <form id="sign-up-form" action="create_user.php" method="POST" class="container-fluid col-sm-8 col-md-6 col-12 mb-5">
        <fieldset>
            <legend>
                <u>Fill In The Fields Below</u>
            </legend>
            <div class="form-group">
                <label for="first-name">First name</label>
                <input required name="first-name" class="form-control" type="text" id="first-name">
            </div>
            <div class="form-group">
                <label for="last-name">Last name</label>
                <input required name="last-name" class="form-control" type="text" id="last-name">
            </div>
            <div class="form-group">
                <label for="username">Username</label>
                <input required name="username" class="form-control" type="text" id="username">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input required name="email" class="form-control" type="email" id="email">
            </div>
            <div class="form-group">
                <label for="date-of-birth">Date of birth</label>
                <input required name="date-of-birth" class="form-control" type="date" id="date-of-birth">
            </div>
            <!-- unmatched passwords alert -->
            <div id="unmatched-passwords-alert" class="alert alert-warning my-2">
                <p>
                    The two passwords do not match
                </p>
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input required name="password" class="form-control" type="password" id="password">
            </div>
            <div class="form-group">
                <label for="password2">Confirm Password</label>
                <input name="password2" class="form-control" type="password" id="password2">
            </div>
            <input type="submit" value="Create Account">
        </fieldset>
    </form>
    
    <?php

    include "../templates/footer.php";
    include "../templates/js.html";

    ?>
</body>
</html>