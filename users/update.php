<?php

include_once "../utils/User.php";
include_once "../utils/Message.php";

if(!isset($_SESSION)) {
    session_start();
}

$postData = $_POST;

$errorMessages = [];

// create the error string
$error = "The following fields cannot be blank: ";

// veriable for checking if there is an error
$hasError = false;

// ensure the required items are not blank
foreach($postData as $postItem => $value) {
    if($postItem != "bio" && $postItem != "phone" && empty($value)) {
        $error .= $postItem.", ";
        $hasError = true;
    }
}

// strip the trailing comma and blank space from the error string
$refinedError = substr($error, 0, sizeof($error) - 3);

// display error if there are errors
if($hasError) {
    echo($refinedError);die();
    Message::error($refinedError);
    header("location: /users/update_profile.php", 400);
}

// create the user if there are no errors
User::update($postData);

?>