<?php

include_once "../utils/DBConnector.php";
include_once "../utils/Message.php";

$name = $_POST["name"];
$email = $_POST["email"];
$postId = empty($_POST["post-id"]) ? null : $_POST["post-id"];

if(empty(trim($name)) || empty(trim($email))) {
	Message::error("Both Fields Are Required");
	header("location: /posts/post-detail.php?id=$postId");
}

$dbh = DBConnector::createConnection();

try {
	$sth = $dbh->prepare("
		INSERT INTO
			subscription(name, email)
		VALUES(?,?)
	");

	$sth->execute(array($name, $email));

	// close db connection
	$dbh = null;

	Message::success("Thank You For Subscribing To Our Newsletter");
	if(!empty($postId)) {
		header("location: /posts/post-detail.php?id=$postId");
	} else {
		header("location: /");
	}
} catch(PDOException $e) {
	echo "Message: ".$e->getMessage()."<br>";
}

?>