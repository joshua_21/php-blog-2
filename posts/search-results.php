<?php

include_once "../utils/Post.php";

$searchKeyword = $_GET["q"];

// if page number is empty, search first page
$pageNumber = empty($_GET["page"]) ? 1 : $_GET["page"] ;

$numPosts = Post::totalNumber($searchKeyword);

// state the number of pages
if($numPosts < 10) {
	// round down to obtain one page if posts are less than 10
	$numPages = floor($numPosts/10);
} else {
	// add one more page if the posts are more than 10
	$numPages = ceil($numPosts/10);
}

$searchResults = Post::getPosts($searchKeyword, $pageNumber);

?>

<!DOCTYPE html>
<html>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<?php include "../templates/css.html"; ?>
<head>
	<title>Search Results For '<?php echo $searchKeyword ?>'</title>
</head>
<body>
<?php include_once "../templates/header.php"; ?>

<div class="container">
	<div class="row">
		<?php
		if(empty($searchResults)) {
			echo "<p>Sorry, No Posts Match Your Search</p>";
		} else {
			foreach($searchResults as $post) {
				$id = $post->id;
				$image = $post->thumbnail_url;
				$title = $post->title;

				echo '
				<div class="col-sm-6 col-md-4 col-lg-3 my-1 rounded">
					<div class="border px-1 py-1">
						<a href="/posts/post-detail.php?id='.$id.'" class="rounded">
							<img src="'.$image.'" height="100" alt="'.$title.'\'s image">
							<p>'.$title.'</p>
						</a>
					</div>
				</div>
				';
			}
		}
		?>
	</div>
	<ul class="pagination justify-content-center mt-4 pagination-sm">
			<?php
			// paginate only if the posts are greater than 10
			if($numPosts > 10) {
				if($pageNumber > 1) {
					// page has previous page
					// first page
					echo "<li class='page-item'><a class='page-link' href='/posts/search-results.php?q=$searchKeyword&page=1'>First</a></li>";

					// previous page
					$previousPage = $pageNumber - 1;
					echo "<li class='page-item'><a class='page-link' href='/posts/search-results.php?q=$searchKeyword&page=$previousPage'>&laquo;</a></li>";
				}

				// display other pages
				for($i = 1; $i <= $numPages ; ++$i) {
					if($i == $pageNumber) {
						// the page we are on is the current page
						echo "<li class='page-item active'><a class='page-link' class='page-item' href='/posts/search-results.php?q=$searchKeyword&page=$pageNumber'>$i</a></li>";
					} elseif($i > $pageNumber-3 && $i < $pageNumber+3) {
						// pages that are two or less than two steps from the current page
						echo "<li class='page-item'><a class='page-link' class='page-item' href='/posts/search-results.php?q=$searchKeyword&page=$i'>$i</a></li>";
					}
				}

				if($pageNumber != $numPages) {
					// page is not the last page
					// next page
					$next = $pageNumber + 1;
					echo "<li class='page-item'><a class='page-link' href='/posts/search-results.php?q=$searchKeyword&page=$next'>&raquo;</a></li>";
					// last page
					echo "<li class='page-item'><a class='page-link' href='/posts/search-results.php?q=$searchKeyword&page=$numPages'>Last</a></li>";
				}
			}
			?>
		</li>
	</ul>
</div>

<?php
include "../templates/js.html";
include "../templates/footer.php";
?>
</body>
</html>