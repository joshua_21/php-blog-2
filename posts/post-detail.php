<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php
    include "../templates/css.html";
    include_once "../utils/Message.php";
    include_once "../utils/Post.php";
    include_once "../utils/Comment.php";

    $postId = $_GET["id"];

    if(empty($postId)) {
        header("location: /404.php", 404);
    }

    $thePost = Post::getDetails($postId);

    // parse the items out
    $title = $thePost["title"];
    $content = $thePost["content"];
    $lastUpdated = $thePost["lastUpdated"];
    $thumbnailUrl = $thePost["thumbnailUrl"];
    $categories = $thePost["categories"];
    $username = $thePost["userName"];
    $views = $thePost["views"];

    if(!isset($_SESSION)) {
        session_start();
    }

    if(!empty($_COOKIE["userId"]) && $_SESSION["isAuthenticated"] == true) {
        $userLoggedIn = true;
    } else {
        $userLoggedIn = false;
    }

    ?>
    <title><?php echo $title; ?></title>
</head>
<body>
<?php include_once "../templates/header.php"; ?>
<div class="mx-2">
    <?php
    if(!isset($_SESSION)) {
        session_start();
    }

    // set variable for checking whether the user is logged in
    if(!empty($_COOKIE["userId"]) && $_SESSION["isAuthenticated"] == true) {
        $isLoggedIn = true;
    } else {
        $isLoggedIn = false;
    }

    // check if user is authenticated
    if($isLoggedIn) {
        if(!empty($_COOKIE["userId"])) {
            $userId = $_COOKIE["userId"];
        }

        // check whether the post belongs to the current user
        if(Post::isOwner($postId, $userId)) {
            echo '
            <!-- action buttons -->
            <div class="container">
                <div class="row">
                    <div class="btn">
                        <a href="edit-post.php?id='.$postId.'">Edit</a> | 
                        <a href="#" id="delete-btn">Delete</a>
                    </div>
                </div>
            </div>
            ';
        }
    }

    // element for easy fetching of the post id
    echo '<span id="post-id-holder">'.$postId.'</span>';
    ?>

        <!-- delete popup -->
        <div class="delete-popup">
            <p>Delete the post?</p>
            <span class="d-flex">
                <form method="POST" action="delete-post.php" class="form-inline mr-auto ml-2 ml-md-4">
                    <input type="hidden" value="<?php echo $postId; ?>" name="post-id">
                    <button class="btn btn-sm btn-danger" type="submit">Yes</button>
                </form>
                <button class="btn btn-sm btn-success ml-auto mr-2 mr-md-4" id="cancel-delete-btn">No</button>
            </span>
        </div>

<?php

// display alert
Message::show_message();

// display the post
echo '
<!-- post title, author -->
<div class="container">
    <div class="row">
        <h1 class="mr-auto">' . $title . '</h1>
        <span class="ml-auto">By <b class="lead">'. $username .'</b> on ' . $lastUpdated . '</span>
    </div>
</div>

<!-- post content -->
<div class="container">
    <div class="row">
        <img class="post-image border border-secondary" alt="'.$title.'\'s image" src="'.$thumbnailUrl.'" height="300">
    </div>
</div>
<div class="container">
    <div class="row">
        <p>
        ' . $content . '
        </p>
    </div>
</div>
';

// header of the category section
echo '
<!-- post categories -->
<div class="container mb-4">
<h2><u>Post Categories</u></h2>
    <div class="row">
';

// display categories
foreach($categories as $category) {
    echo '
    <div class="col-4 col-sm-2">
        <a href="posts-per-category.php?cat='.$category.'">'.$category.'</a>
    </div>';
}

// close off categories
echo '
    </div>
</div>
';

// increment the number of views
Post::incrementViews($postId, $views);

?>

<!-- comment button -->
        <div class="container">
            <div class="row">
                <?php

                    if($isLoggedIn) {
                        echo '
                        <button class="btn btn-outline-secondary" id="comment-btn">
                            comment
                        </button>
                        ';
                    } else {
                        echo '
                        <a class="btn btn-outline-info" href="/users/login.php?next=comment&id='.$postId.'">
                            Log In To Comment
                        </a>
                        ';
                    }
                ?>
            </div>
        </div>

        <!-- comment form -->
        <div class="container mt-3" id="comment-box">
            <div class="row">
                <form method="POST" action="/comments/create-comment.php">
                    <input type="hidden" value="<?php echo $title; ?>" name="post-title">
                    <div class="form-group">
                        <textarea name="comment-content" class="form-control" placeholder="Enter the comment here"></textarea>
                    </div>
                    <input type="hidden" name="post-id" value="<?php echo $postId; ?>">
                    <input class="" type="submit" value="Post Comment">
                </form>
            </div>
        </div>

        <!-- comments list -->
        <div class="container mt-4">
            <h2><u>Comments</u></h2>
        </div>
        <?php
        $comments = Post::fetchComments($postId);

        foreach($comments as $comment) {
            $datePosted = $comment->last_updated;
            $userId = $comment->user_id;
            $content = $comment->content;
            $commentId = $comment->id;
            if($isLoggedIn) {
                $loggedInUser = $_COOKIE["userId"];
            } else {
                $loggedInUser = null;
            }

            // format the date
            $mysqlDate = DateTime::createFromFormat("Y-m-d H:i:s", $datePosted);
            $dateToDisplay = $mysqlDate->format("d/m/Y");

            $user = User::getDetails($userId);

            // check if the user exists
            if(empty($user)) {
                $userName = "Deleted User";
            } else {
                $userName = $user["userName"];
            }

            // display the comment
            echo '<div class="border p-2 container my-2" id="comment-box-'.$commentId.'">';

            if(Comment::isOwner($commentId, $loggedInUser)) {
                echo '
                <div class="d-flex comment-box-'.$commentId.'">
                <form method="POST" action="/comments/delete-comment.php">
                    <input type="hidden" name="comment-id" value="'.$commentId.'">
                    <input type="hidden" name="post-id" value="'.$postId.'">
                    <input type="submit" value="delete" class="btn btn-sm btn-outline-danger mr-2">
                </form>
                <button class="btn btn-outline-primary btn-sm update-comment-trigger" id="'.$commentId.'">update</button>
                </div>
                ';
            } elseif($userLoggedIn) {
                echo '
                <div class="d-flex comment-box-'.$commentId.'">
                <button class="btn btn-outline-success btn-sm comment-reply-trigger" id="'.$commentId.'">Reply</button>
                </div>
                ';
            }

            echo '
            <p class="col-12">' . $userName . ' on ' . $dateToDisplay . '</p>
            <p class="col-12" id="content-'.$commentId.'">
                ' . $content . '
            </p>

            <!-- comment reply box -->
            <div class="container mx-sm-4 reply-comment-form" id="reply-comment-'.$commentId.'">
                <form method="POST" action="/comments/create-comment.php">
                    <input type="hidden" name="comment-id" value="'.$commentId.'">
                    <input type="hidden" name="post-id" value="'.$postId.'">
                    <div class="form-group">
                        <textarea name="comment-content" class="form-control" placeholder="enter reply here"></textarea>
                    </div>
                    <input type="submit" value="Submit Reply">
                </form>
            </div>
            ';
            $comments = Comment::getReplies($commentId);

            // comment replies
            if(!empty($comments)) {
                echo "<p class='lead'><u>Replies</u></p>";
                foreach($comments as $comment) {
                    echo '
                    <div class="border container p-1 my-1">
                        <div class="d-flex">
                            <p>By <span>'.$comment->userName.'</span> on <span>'.$comment->datePosted.'</span>
                            </p>
                    ';

                    if($isLoggedIn) {
                        $userId = $_COOKIE["userId"];
                    } else {
                        $userId = null;
                    }
                    
                    $commentId = $comment->id;
                    if(Comment::isOwner($commentId, $userId)) {
                        echo '
                        <form class="form-inline ml-auto mr-2 mr-sm-5" method="POST" action="/comments/delete-comment.php">
                            <input type="hidden" value="'.$commentId.'" name="comment-id">
                            <input type="hidden" value="'.$postId.'" name="post-id">
                            <button type="submit" class="btn btn-sm btn-outline-danger">Delete</button>
                        </form>
                        ';
                    }
                    echo '
                        </div>
                        <p class="px-1">
                            '.$comment->content.'
                        </p>
                    </div>
                    ';
                }
            }
            echo '
            </div>
            ';
        }
        include "../templates/user-subscription-form.php";
        ?>
<?php
include "../templates/js.html";
include "../templates/footer.php";

?>   
</body>
</html>