<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php
    include_once "../templates/css.html";
    include_once "../utils/Post.php";

    $catName = $_GET["cat"];

    if(empty($catName)) {
        header("location: /404.php", 404);
    }

    $catExists = Post::categoryExists($catName);

    if(!$catExists) {
        header("location: /404.php", 404);
    }

    ?>
    <title><?php echo $catName; ?></title>
</head>
<body>
<?php

include_once "../templates/header.php";

$postList = Post::fetchByCategory($catName);

if(empty($postList)) {
    echo '<p class="mt-5 alert alert-info">Sorry, no post exists in that category.</p>';
} else {
    echo '
    <div class=" container">
    <div class="row">
    ';
    foreach($postList as $post) {
        echo '
        <div class="card col-12 col-md-3 col-sm-5 py-3 border-white">
            <a href="post-detail.php?id='. $post->id .'">
            <img src="' . $post->thumbnail_url . '" width="100">
            <div class="card-body">
                <p>' . $post->title . '</p>
            </div>
            </a>
        </div>
        ';
    }
    echo '
    </div>
    </div>';
}

include_once "../templates/js.html";
?>
</body>
</html>