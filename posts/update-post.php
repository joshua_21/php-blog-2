<?php

include_once "../utils/Post.php";

if(!isset($_SESSION)) {
	session_start();
}

$userId = $_COOKIE["userId"];
$isAuthenticated = $_SESSION["isAuthenticated"];

// ensure user is logged in
if(!empty($userId) && $isAuthenticated) {
	$postId = $_POST["post-id"];
	// check if the user is the owner of the post
	if(Post::isOwner($postId, $userId)) {
		// update the post
		Post::update($postId);
		header("location: /posts/post-detail.php?id=$postId", 200);
	} else {
		// redirect to 403 page
		header("location: /403.php", 403);
	}
}

?>