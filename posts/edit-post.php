<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php
        include "../templates/css.html";
        include_once "../utils/Post.php";

        session_start();
        if(!$_SESSION["isAuthenticated"]) {
            header("Location: /users/login.php");
        }

        $postId = $_GET["id"];

        $thePost = Post::getDetails($postId);

        // print_r($thePost);die();

        if(!isset($thePost)) {
        	header("location: /404.php", 404);
        }

        // retrieve individual items
        $title = $thePost["title"];
        $content = $thePost["content"];
        $thumbnailUrl = $thePost["thumbnailUrl"];
        $categories = $thePost["categories"];

        // display checked categories
        function displayCurrentCats($categories) {
        	// set the category count for appropriate styling
            $catCount = 0;
        	// display the categories appropriately
        	foreach($categories as $category) {
        		switch($category) {
        			case "business":
        				echo '
        				<label for="business">Business</label>
	                    <input checked id="business" type="checkbox" name="categories[]" value="business">
	                    <br>
        				';
        				break;
        			case "education":
        				echo '
        				<label for="education">Education</label>
	                    <input checked id="education" type="checkbox" name="categories[]" value="education">
	                    <br>
        				';
        				break;
        			case "fashion":
        				echo '
        				<label for="fashion">Fashion</label>
	                    <input checked id="fashion" type="checkbox" name="categories[]" value="fashion">
	                    <br>
        				';
        				break;
        			case "food":
            			echo '
        				<label for="food">Food</label>
                		<input checked id="food" type="checkbox" name="categories[]" value="food">
            			';
            			break;
        			case "health":
            			echo '
            			<label for="health">Health</label>
	                    <input checked id="health" type="checkbox" name="categories[]" value="health">
	                    <br>
            			';
            			break;
            		case "lifestyle":
            			echo '
            			<label for="lifestyle">Lifestyle</label>
	                    <input checked id="lifestyle" type="checkbox" name="categories[]" value="lifestyle">
	                    <br>
            			';
            			break;
            		case "politics":
            			echo '
            			<label for="politics">Politics</label>
	                    <input checked id="politics" type="checkbox" name="categories[]" value="politics">
	                    <br>
            			';
            			break;
            		case "sports":
            			echo '
            			<label for="sports">Sports</label>
                		<input checked id="sports" type="checkbox" name="categories[]" value="sports">
                		<br>
            			';
            			break;
        		}
        	}

        }


        // create html code for the categories post doesn't belong to
        function uncheckedCats($categories) {
        	// list all the required categories
        	$allCats = array(
        		"business",
        		"education",
        		"lifestyle",
        		"politics",
        		"sports",
        		"food",
        		"health",
        		"fashion"
        	);

        	// get the categories that are currently present in the post
        	$currentCats = $categories;

        	// we will add categories to which the post doesn't belong here
        	$notInCurrentCats = array();

        	// loop through all categories first to make sure all categories are checked
        	foreach($allCats as $cat) {
        		$belongsToCat = false;
        		// loop through current categories to eliminate those the post belongs to
        		foreach($categories as $currentCategory) {
        			if($cat == $currentCategory) {
        				$belongsToCat = true;
        			}
        		}

        		if(!$belongsToCat) {
	        		// add to the list of categories that the post doesn't belong to
	        		$notInCurrentCats[$cat] = $cat;
	        	}
        	}

        	// compose the checkboxes for the categories to which the post doesn't belong
        	$absentCatsCheckboxes = "";

        	foreach($notInCurrentCats as $category) {
        		switch($category) {
        			case "business":
        				$absentCatsCheckboxes.='
        				<label for="business">Business</label>
	                    <input id="business" type="checkbox" name="categories[]" value="business">
	                    <br>
        				';
        				break;
        			case "education":
        				$absentCatsCheckboxes.='
        				<label for="education">Education</label>
	                    <input id="education" type="checkbox" name="categories[]" value="education">
	                    <br>
        				';
        				break;
        			case "fashion":
        				$absentCatsCheckboxes.='
        				<label for="fashion">Fashion</label>
	                    <input id="fashion" type="checkbox" name="categories[]" value="fashion">
	                    <br>
        				';
        				break;
        			case "food":
            			$absentCatsCheckboxes.='
        				<label for="food">Food</label>
                		<input id="food" type="checkbox" name="categories[]" value="food">
                		<br>
            			';
            			break;
        			case "health":
            			$absentCatsCheckboxes.='
            			<label for="health">Health</label>
	                    <input id="health" type="checkbox" name="categories[]" value="health">
	                    <br>
            			';
            			break;
            		case "lifestyle":
            			$absentCatsCheckboxes.='
            			<label for="lifestyle">Lifestyle</label>
	                    <input id="lifestyle" type="checkbox" name="categories[]" value="lifestyle">
	                    <br>
            			';
            			break;
            		case "politics":
            			$absentCatsCheckboxes.='
            			<label for="politics">Politics</label>
	                    <input id="politics" type="checkbox" name="categories[]" value="politics">
	                    <br>
            			';
            			break;
            		case "sports":
            			$absentCatsCheckboxes.='
            			<label for="sports">Sports</label>
                		<input id="sports" type="checkbox" name="categories[]" value="sports">
                		<br>
            			';
            			break;
        		}
        	}

        	return $absentCatsCheckboxes;

        }

    ?>
    <title>Edit Post</title>
</head>
<body>
    <?php
    include_once "../templates/header.php";
    include_once "../utils/Message.php";
    Message::show_message();
    ?>
    <div class="container">
    <h2>
        <u>Edit Post</u>
    </h2>
    <form method="POST" action="update-post.php" enctype="multipart/form-data">
        <div class="form-group">
            <label for="post-title" class="lead">Title</label>
            <input required value="<?php echo $title; ?>" class="form-control" type="text" name="title" id="post-title">
        </div>
        <input type="hidden" value="<?php echo $postId; ?>" name="post-id">
        <input type="hidden" value="<?php echo $_COOKIE['userId']; ?>" name="user-id">
        <div class="form-group">
            <label for="post-thumbnail" class="lead">Thumbnail:</label>
            <input class="form-control border-0" type="file" name="thumbnail" id="post-thumbnail" accept=".jpg,.jpeg,.png">
        </div>
        <hr>
        <div class="form-group container-fluid mt-2">
            <h2>
                categories
            </h2>
            <div class="row">
                <div class="col-sm-6">
                	<?php
                	echo "<h2><u>Current Categories</u></h2>";
	            	// categories that the post belongs to
	            	displayCurrentCats($categories);
	            	?>
                </div>
                <div class="col-sm-6">
                	<?php
                	echo "<h2><u>Other Categories</u></h2>";
                    // categories that post doesn't belong to
	            	echo uncheckedCats($categories);
	            	?>             
                </div>
            </div>
        </div>
        <hr>
        <div class="form-group">
            <label for="post-content" class="lead">Body:</label>
            <textarea required class="form-control ckeditor" id="post-content" name="content">
                <?php echo $content; ?>
            </textarea>
        </div>
        <input type="submit" value="Post">
    </form>
    </div>
    <?php
    include_once "../templates/footer.php";
    include_once "../templates/js.html";
    ?>
</body>
</html>