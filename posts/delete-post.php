<?php

include_once "../utils/Post.php";
include_once "../utils/DBConnector.php";

if(!isset($_SESSION)) {
	session_start();
}

$postId = $_POST["post-id"];

// check that user is authenticated
if($_COOKIE["userId"] && $_SESSION["isAuthenticated"]) {
	// create db connection
	$dbh = DBConnector::createConnection();

	Post::delete($postId);

	// close connection
	$dbh = null;
} else {
	print_r($_SESSION);
	echo "Not logged in";die();
	header("location: /403.php", 403);
}

?>