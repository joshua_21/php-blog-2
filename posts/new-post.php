<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php
        include "../templates/css.html";

        session_start();
        if(!$_SESSION["isAuthenticated"]) {
            header("Location: /users/login.php");
        }
    ?>
    <title>+New Post</title>
</head>
<body>
    <?php
    include_once "../templates/header.php";
    include_once "../utils/Message.php";
    Message::show_message();
    ?>
    <div class="container">
    <h2>
        <u>New Post</u>
    </h2>
    <form method="POST" action="add-post.php" enctype="multipart/form-data">
        <div class="form-group">
            <label for="post-title" class="lead">Title</label>
            <input required class="form-control" type="text" name="title" id="post-title">
        </div>
        <input type="hidden" value="<?php echo $_COOKIE['userId']; ?>" name="user-id">
        <div class="form-group">
            <label for="post-thumbnail" class="lead">Thumbnail:</label>
            <input class="form-control border-0" type="file" name="thumbnail" id="post-thumbnail" accept=".jpg,.jpeg,.png">
        </div>
        <hr>
        <div class="form-group container-fluid mt-2">
            <h2>
                categories
            </h2>
            <div class="row">
                <div class="col-sm-6">
                    <label for="business">Business</label>
                    <input id="business" type="checkbox" name="categories[]" value="business">
                    <br>
                    <label for="education">Education</label>
                    <input id="education" type="checkbox" name="categories[]" value="education">
                    <br>
                    <label for="fashion">Fashion</label>
                    <input id="fashion" type="checkbox" name="categories[]" value="fashion">
                    <br>
                    <label for="food">Food</label>
                    <input id="food" type="checkbox" name="categories[]" value="food">
                </div>
                <div class="col-sm-6">
                    <label for="health">Health</label>
                    <input id="health" type="checkbox" name="categories[]" value="health">
                    <br>
                    <label for="lifestyle">Lifestyle</label>
                    <input id="lifestyle" type="checkbox" name="categories[]" value="lifestyle">
                    <br>
                    <label for="politics">Politics</label>
                    <input id="politics" type="checkbox" name="categories[]" value="politics">
                    <br>
                    <label for="sports">Sports</label>
                    <input id="sports" type="checkbox" name="categories[]" value="sports">
                </div>
            </div>
        <hr>
        <div class="form-group">
            <label for="post-content" class="lead">Body:</label>
            <textarea required class="form-control ckeditor" id="post-content" name="content">
            </textarea>
        </div>
        <input type="submit" value="Post">
    </form>
    </div>
    <?php
    include_once "../templates/footer.php";
    include_once "../templates/js.html";
    ?>
</body>
</html>