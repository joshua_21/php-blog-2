<?php

include_once "User.php";

if(!isset($_SESSION)) {
	session_start();
}

if(!empty($_COOKIE["userId"]) && $_SESSION["isAuthenticated"]) {
	$isLoggedIn = true;
} else {
	$isLoggedIn = false;
}

if($isLoggedIn) {
	$userId = $_COOKIE["userId"];

	// check whether user is an admin
	$isAdmin = User::isAdmin($userId);

	if($isAdmin) {
		$loggedInAsAdmin = true;
	} else {
		$loggedInAsAdmin = false;
	}
}

?>