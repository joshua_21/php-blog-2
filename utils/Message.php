<?php

class Message {
    static function error($message) {
        if(!isset($_SESSION)) {
            session_start();
        }
        $_SESSION["message"] = ["error", $message];
    }

    static function info($message) {
        if(!isset($_SESSION)) {
            session_start();
        }

        $_SESSION["message"] = ["info", $message];
    }

    static function success($message) {
        if(!isset($_SESSION)) {
            session_start();
        }

        $_SESSION["message"] = ["success", $message];
    }

    static function warning($message) {
        if(!isset($_SESSION)) {
            session_start();
        }

        $_SESSION["message"] = ["warning", $message];
    }

    static function show_message() {
        if(!isset($_SESSION)) {
            session_start();
        }

        $sessionMessage = empty($_SESSION["message"]) ? "" : $_SESSION["message"];

        // ensure there is a message to be displayed
        if(!empty($sessionMessage)) {
            $level = $sessionMessage[0];
            $message = $sessionMessage[1];

            // display the message
            if(!empty($level) && !empty($message)) {
                if($level == "error") {
                    echo '<p class="alert alert-danger rounded-0">'.$message.'</p>';
                } else {
                    echo '<p class="alert alert-'.$level.' rounded-0">'.$message.'</p>';
                }
            }
        }

        // destroy the message[s] to avoid re-displaying them
        unset($_SESSION["message"]);
    }
}

?>