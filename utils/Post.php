<?php

include_once "DBConnector.php";
include_once "Message.php";
include_once "ParamChecker.php";

class Post {
    static $paramsValid;

    static function categoryExists($passedCategory) {
        // list categories to compare with
        $categories = ["business", "education", "fashion", "food", "health", "lifestyle", "politics", "sports"];

        // verify the category exists
        foreach($categories as $category) {
            if($category == $passedCategory) {
                return true;
            }
        }
        return false;
    }
    
    static function checkParams($params, $message, $next) {
        // check for empty parameters
        self::$paramsValid = ParamChecker::paramsValid($params);

        // there is an error in the parameters
        if(!self::$paramsValid) {
            Message::error($message);
            header("location: /posts/$next".".php", 400);
        }
    }

    static function create($params) {
        // check params
        self::checkParams($params, "All fields are required", "new-post");

        // params are ok, proceed to create post
        $title = $params["title"];
        $content = $params["content"];
        $userId = $params["user-id"];
        $categories = $params["categories"];

        // get the file url
        $imageUrl = self::saveImage($title);

        // convert the categories into a string so they can be added to the db
        $concatenatedCats = implode(",", $categories);

        // everything is okay, proceed to create post
        $conn = DBConnector::createConnection();

        try {
            // create statement handler
            $sth = $conn->prepare(
                "INSERT INTO post(
                    title,
                    content,
                    date_posted,
                    user_id,
                    thumbnail_url,
                    categories
                )
                VALUES(?,?,NOW(),?,?,?)
                "
            );

            // check whether the post was created successfully
            $creationSucceeded = $sth->execute(
                array($title, $content, $userId, $imageUrl, $concatenatedCats)
            );

            // get id of newly created post
            $sthId = $conn->prepare("SELECT id FROM post WHERE title=?");
            $sthId->execute(array($title));
            $post = $sthId->fetch(PDO::FETCH_OBJ);
            $theId = $post->id;

            // close the db connection
            $conn = null;

            // give user response upon success or failure
            if($creationSucceeded) {
                Message::success("The Post Was Created Successfully");
                header("location: /posts/post-detail.php?id=$theId", 201);
            } else {
                Message::error("Sorry, an error occured during post creation,<br>
                try again and contact customer service if the problem persists");
                header("location: /users/login.php", 201);
            }
        } catch(PDOException $e) {
            echo "Message: ".$e->getMessage()."<br>";
            echo "Code: ".$e->getCode()."<br>";
        }
    }

    static function delete($postId) {
        // get the user id
        $userId = $_COOKIE["userId"];

        // check if the user id is a valid one
        if(empty($postId) || gettype((int)$postId) != "integer") {
            Message::error("Post Id Cannot Be Empty");
            header("location: /posts/post-detail.php?id=$postId");
        }

        // check if the user exists
        $dbh = DBConnector::createConnection();

        // check whether the user is the owner of the post
        $sth = $dbh->prepare("SELECT title FROM post WHERE user_id=? AND id=?");

        $isPostOwner = $sth->execute(array($userId, $postId));

        if($isPostOwner) {
            // prepare delete statement
            $sthDel = $dbh->prepare("DELETE FROM post WHERE id=?");

            // delete the related image
            self::deleteImage($postId);

            // execute delete statement
            $sthDel->execute(array($postId));

            // close the connection
            $dbh = null;

            Message::success("The Post Was Deleted Successfully");
            header("location: /", 200);
        } else {
            // close the connection
            $dbh = null;

            header("location: /403.php", 403);
        }
    }

    static function deleteImage($postId) {
        $dbh = DBConnector::createConnection();
        try {

            $sth = $dbh->prepare("
                SELECT thumbnail_url FROM post
                WHERE
                    id=?
            ");

            // get the image url from the database
            $sth->execute(array($postId));

            $post = $sth->fetch(PDO::FETCH_OBJ);

            $theImgUrl = $post->thumbnail_url;

            // close db connection
            $dbh = null;

            echo $theImgUrl;die();
            // ensure the image is not the default one
            if($theImgUrl == "/dummy_images/p1.jpg" || $theImgUrl == "/media/post_images/default/p1.jpg") {
                return;
            } else {
                // if the image isn't the default one, delete it
                $ROOT_DIR = $_SERVER["DOCUMENT_ROOT"];

                $imgLocation = "$ROOT_DIR/$theImgUrl";

                // check if the file was deleted successfully
                if(unlink($imgLocation)) {
                    Message::success("Deleted Image Successfully<br>");
                } else {
                    Message::error("Error occured while deleting the file");
                }
            }

        } catch(PDOException $e) {
            echo "Message: ".$e->getMessage()."<br>";
            echo "Code: ".$e->getCode()."<br>";
        }
    }

    static function fetchComments($postId) {
        $dbh = DBConnector::createConnection();

        try {
            $sth = $dbh->prepare("
                SELECT
                    user_id,
                    content,
                    last_updated,
                    id
                FROM
                    comment
                WHERE
                    post_id = :postId
                ORDER BY date_posted DESC
            ");

            $sth->bindParam(":postId", $postId, PDO::PARAM_INT);
            $sth->execute();

            $comments = $sth->fetchAll(PDO::FETCH_OBJ);

            // close db connection
            $dbh = null;

            return $comments;
        } catch(PDOException $e) {
            echo "Message: ".$e->getMessage()."<br>";
            echo "Code: ".$e->getCode()."<br>";
        }
    }

    static function incrementViews($postId, $currentViews) {
        $dbh = DBConnector::createConnection();

        try {
            $sth = $dbh->prepare("
                UPDATE post
                SET
                    times_viewed=:timesViewed
                WHERE
                    id=:postId
            ");

            $incrementedViews = $currentViews + 1;
            $sth->bindParam(":postId", $postId, PDO::PARAM_INT);
            $sth->bindParam(":timesViewed", $incrementedViews, PDO::PARAM_INT);

            $sth->execute();

            // close db connection
            $dbh = null;
        } catch(PDOException $e) {
            echo "Message: ".$e->getMessage()."<br>";
            echo "Code: ".$e->getCode()."<br>";
        }
    }

    static function isOwner($postId, $loggedInUserId) {
        $dbh = DBConnector::createConnection();

        try {
            // fetch the user id of post from db
            $sth = $dbh->prepare("
                SELECT
                    user_id
                FROM
                    post
                WHERE
                    id=?
            ");

            $sth->execute(array($postId));

            $row = $sth->fetch(PDO::FETCH_OBJ);

            $userId = $row->user_id;

            // close db connection
            $dbh = null;

            // check that the current user id matches that one of the post
            if($userId == $loggedInUserId) {
                return true;
            } else {
                return false;
            }
        } catch(PDOException $e) {
            echo "Message: ".$e->getMessage()."<br>";
            echo "Code: ".$e->getCode()."<br>";
        }
    }

    static function saveImage($postName) {
        $ROOT_DIR = $_SERVER["DOCUMENT_ROOT"];

        $theImage = $_FILES["thumbnail"];

        // set a boolean for checking whether the file is fit to be uploaded
        $okToUpload = true;

        $fileMime = explode("/",$theImage["type"])[1];

        // get the file type
        $fileType = strtolower($fileMime);

        // return default file path if no file was submitted by the user
        if(empty($theImage["name"]) || trim($theImage["name"]) == "") {
            return "/media/post_images/default/p1.jpg";
        }

        // check whether the file is a valid one
        if(isset($theImage)) {
            // get the image size of the file
            // !the current maximum upload size is 2mb
            $imageSize = getimagesize($theImage["tmp_name"]);

            if(!$imageSize) {
                $okToUpload = false;
            }
        }

        // give the file name that it should be saved under
        $targetFile = "$ROOT_DIR/media/post_images/".$postName.".".$fileType;

        // check that the file doesn't exist
        if(file_exists($fileType)) {
            $okToUpload = false;
        }

        // check that the file is of the required type
        $allowedExtensions = ["jpg", "jpeg", "png"];

        foreach($allowedExtensions as $ext) {
            if($fileType != $ext) {
                $okToUpload = true;
                break;
            }
            
            if($fileType != $ext &&  $ext == "png") {
                // if the end of the extension has been reached without showing any match
                $okToUpload = false;
            }
        }

        // everything is okay, upload the image
        if($okToUpload) {
            $uploaded = move_uploaded_file($theImage["tmp_name"], $targetFile);
            if($uploaded) {
                Message::success("The image was saved successfully");
                return "/media/post_images/".$postName.".".$fileType;
            } else {
                // error while uploading
                Message::error("Sorry, an error occured while uploading the image");
                return "/media/post_images/p1.jpg";
            }
        } else {
            // the file is not fit for upload
            return "/media/post_images/p1.jpg";
        }
    }

    static function fetchByCategory($catName) {
        // open db connection
        $dbh = DBConnector::createConnection();
        try {
            $sth = $dbh->prepare("SELECT title, thumbnail_url, id FROM post WHERE categories LIKE :catName");
            $sth->bindValue(":catName", "%$catName%");
            $sth->execute();

            // close db connection
            $dbh = null;

            $posts = $sth->fetchAll(PDO::FETCH_OBJ);

            // close db connection
            $dbh = null;

            if(!empty($posts)) {
                return $posts;
            } else {
                return null;
            }
        } catch(PDOException $e) {
            echo "Message: ".$e->getMessage()."<br>";
            echo "Code: ".$e->getCode()."<br>";
        }
    }

    static function formatDate($theDate) {
        $tempDate = DateTime::createFromFormat("Y-m-d H:i:s", $theDate);
        $outputDate = $tempDate->format("d-F-Y");

        // separate the date for appropriate formatting
        $explodedDate = explode("-", $outputDate);

        // get the individual date components to aid in consilidation later on
        $day = $explodedDate[0];
        $month = $explodedDate[1];
        $year = $explodedDate[2];

        // assign subscript to the date
        $lastNum = substr($day, -1);
        if($lastNum == "1") {
            $day = $day . "<sup>st</sup>";
        } elseif($lastNum == "2") {
            $day = $day . "<sup>nd</sup>";
        } elseif($lastNum == "3") {
            $day = $day . "<sup>rd</sup>";
        } else {
            $day = $day . "<sup>th</sup>";
        }

        // consolidate the date
        $newDate = $day . " " . $month . " " . $year;

        return $newDate;
    }

    static function getDetails($postId) {
        // check if the post exists
        $dbh = DBConnector::createConnection();

        try {
            // prepare the statement handler
            $sth = $dbh->prepare("
                SELECT
                    title,
                    content,
                    last_updated,
                    thumbnail_url,
                    categories,
                    user_id,
                    id,
                    times_viewed
                FROM
                    post
                WHERE
                    id=?
            ");

            $postExists = $sth->execute(array($postId));

            $row = $sth->fetch(PDO::FETCH_OBJ);

            // get the username
            if(!empty($row)) {
                // get the params from the returned item
                $userId = $row->user_id;
                $title = $row->title;
                $content = $row->content;
                $lastUpdated = $row->last_updated;
                $thumbnailUrl = $row->thumbnail_url;
                $categories = $row->categories;
                $views = $row->times_viewed;

                // get the user details from the db
                $sthUser = $dbh->prepare("SELECT username FROM user WHERE id=?");
                
                $sthUser->execute(array($userId));

                $row = $sthUser->fetch(PDO::FETCH_OBJ);

                // check if the user was obtained
                if(!empty($row)) {
                    $userName = $row->username;
                } else {
                    $userName = "Deleted User";
                }
            }

            // close db connection
            $dbh = null;

            // separate the categories for display on the post detail page
            $categories = explode(",", $categories);

            // format the date
            $lastUpdated = self::formatDate($lastUpdated);

            return array("userName"=>$userName, "title"=>$title, "content"=>$content, "lastUpdated"=>$lastUpdated, "thumbnailUrl"=>$thumbnailUrl, "categories"=>$categories, "views"=>$views);

        } catch(PDOException $e) {
            echo "Message: ".$e->getMessage()."<br>";
            echo "Code: ".$e->getCode()."<br>";
        }
    }

    static function getPosts($keyword, $page) {
        // no search parameter
        if(empty($keyword)) {
            return null;
        }

        // get the point at which we should start obtaining the rows
        $offSet = ($page * 10) - 10;

        // for each page, we want to obtain 10 posts
        $numRows = 10;

        $dbh = DBConnector::createConnection();

        try {
            $sth = $dbh->prepare("
                SELECT
                    title,
                    thumbnail_url,
                    id
                FROM
                    post
                WHERE
                    title LIKE :titlePlaceholder
                    OR content LIKE :contentPlaceholder
                ORDER BY
                    last_updated DESC
                LIMIT
                    :offSet,
                    :numRows
            ");

            $sth->bindValue(":titlePlaceholder", "%$keyword%");
            $sth->bindValue(":contentPlaceholder", "%$keyword%");
            $sth->bindParam(":offSet", $offSet, PDO::PARAM_INT);
            $sth->bindParam(":numRows", $numRows, PDO::PARAM_INT);

            $sth->execute();

            $rows = $sth->fetchAll(PDO::FETCH_OBJ);

            // close db connection
            $dbh = null;

            return $rows;
        } catch(PDOException $e) {
            // close db connection
            $dbh = null;
            echo "Message: ".$e->getMessage()."<br>";
            echo "Code: ".$e->getCode()."<br>";

            return null;
        }
    }

    static function updateImage($image, $title, $postId) {
        /**
        *
        * this function will delete the previous image if a new image has been uploaded
        * and retain the old image if there is a problem in the uploading
        */

        // check for the previous name of the post
        $dbh = DBConnector::createConnection();

        try {
            $sth = $dbh->prepare("
                SELECT
                    title,
                    thumbnail_url
                FROM
                    post
                WHERE
                    id=:id
            ");

            $sth->bindParam(":id", $postId, PDO::PARAM_INT);

            $sth->execute();

            $post = $sth->fetch(PDO::FETCH_OBJ);

            $previousTitle = $post->title;
            $thumbnailUrl = $post->thumbnail_url;

            // close db connection
            $dbh = null;

        } catch(PDOException $e) {
            echo "Message: ".$e->getMessage()."<br>";
            echo "Code: ".$e->getCode()."<br>";
        }

        // variable to check whether it is ok to upload the image
        $okToSave = 1;

        // check whether the image is valid
        $check = getimagesize($image["tmp_name"]);

        if($check === false) {
           $okToSave = 0; 
        }

        $ROOT_DIR = $_SERVER["DOCUMENT_ROOT"];

        // get the name of the image
        $imgName = $image["name"];
        // set the directory to which the image should be saved to
        $imageDir = "$ROOT_DIR/media/post_images/$imgName";

        // check for the image type
        $imgExtension = strtolower(pathinfo($imageDir, PATHINFO_EXTENSION));

        // the url that the browser will be using
        $imageUrl = "/media/post_images/$title.$imgExtension";

        if($imgExtension !== "jpg" && $imgExtension !== "jpeg" && $imgExtension !== "png") {
            $okToSave = 0;
        }

        // everything is ok, save the image
        if($okToSave) {
            // the title has been changed hence delete the previous image
            if($previousTitle !== $title) {
                // get the full path of the image
                $imagePath = $ROOT_DIR.$thumbnailUrl;

                // ensure that the image is not the default one
                if($imagePath != "$ROOT_DIR/media/post_images/default/p1.jpg" && $imagePath != "$ROOT_DIR/dummy_images/p1.jpg") {
                    // delete the image
                    unlink($imagePath);
                }
            }

            // get the image file that was uploaded
            $theImageFile = $image["tmp_name"];

            // change the image name so it can conform to the name of the post
            $imageDir = "$ROOT_DIR/media/post_images/$title.$imgExtension";

            $uploadSuccessful = move_uploaded_file($theImageFile, $imageDir);
            if($uploadSuccessful) {
                Message::success("The Image Was Uploaded Successfully");
                return $imageUrl;
            } else {
                return null;
            }
        } else {
            // the title was changed
            if($title !== $previousTitle) {
                // get the path of the previous file
                $previousFilePath = $ROOT_DIR.$thumbnailUrl;

                // get the extension of the previous file
                $extension = pathinfo($previousFilePath, PATHINFO_EXTENSION);

                // state the path of the new file
                $newFilePath = "$ROOT_DIR/media/post_images/$title.$extension";

                // copy the old file to the new location with the title of the new file
                $copiedSuccessfully = copy($previousFilePath, $newFilePath);

                // provide file url for access by the server
                $newFileUrl = "/media/post_images/$title.$extension";

                if($copiedSuccessfully) {
                    // delete the previous file
                    unlink($previousFilePath);
                    return $newFileUrl;
                } else {
                    echo "Not Copied Successfully<br>";
                    die();
                }
            }
        }

    }

    static function topPosts($number) {
        // open db
        $dbh = DBConnector::createConnection();

        try {

            // prepare query
            $sth = $dbh->prepare("
                SELECT
                    title,
                    content,
                    post.id as id,
                    thumbnail_url,
                    last_updated,
                    username as author,
                    profile_pic_url as profile_picture,
                    times_viewed as views
                FROM
                    post
                INNER JOIN
                    user
                WHERE post.user_id = user.id
                ORDER BY
                    times_viewed DESC
                LIMIT :numPosts
            ");

            // bind to prevent syntax errors
            $sth->bindParam(":numPosts", $number, PDO::PARAM_INT);

            $sth->execute();

            // get the returned items
            $posts = $sth->fetchAll(PDO::FETCH_OBJ);

            // check whether any posts were fetched
            if(!isset($posts)) {
                return null;
            } else {
                return $posts;
            }

        } catch(PDOException $e) {
            echo "Message: ".$e->getMessage()."<br>";
            echo "Code: ".$e->getCode()."<br>";
        }
    }

    static function totalNumber($keyword) {
        $dbh = DBConnector::createConnection();

        try {
            $sth = $dbh->prepare("
                SELECT
                    COUNT(*) AS num_posts
                FROM
                    post
                WHERE
                    title LIKE :titlePlaceholder
                    OR content LIKE :contentPlaceholder
            ");

            $sth->bindValue(":titlePlaceholder", "%$keyword%");
            $sth->bindValue(":contentPlaceholder", "%$keyword%");

            $sth->execute();

            $row = $sth->fetch(PDO::FETCH_OBJ);

            $count = $row->num_posts;

            // close db connection
            $dbh = null;

            return $count;
        } catch(PDOException $e) {
            // close db connection
            $dbh = null;

            echo "Message: ".$e->getMessage()."<br>";
            echo "Code: ".$e->getCode()."<br>";
        }
    }

    static function update($postId) {
        // get the post parameters
        $content = $_POST["content"];
        $categories = $_POST["categories"];
        $title = $_POST["title"];
        $postId = $_POST["post-id"];
        $image = $_FILES["thumbnail"];

        // convert the category array into a string that can be stored in the db
        $catString = implode(",", $categories);

        // save the new image
        $imageUrl = self::updateImage($image, $title, $postId);

        // update the details in db
        $dbh = DBConnector::createConnection();

        try {
            // include the thumbnail url if it's not empty
            if(!empty($imageUrl)) {
                $sth = $dbh->prepare("
                    UPDATE post
                    SET
                        title=:title,
                        content=:content,
                        categories=:categories,
                        thumbnail_url=:thumbnailUrl,
                        last_updated=NOW()
                    WHERE
                        id=:postId
                ");

                $sth->bindValue(":content", $content);
                $sth->bindValue(":categories", $catString);
                $sth->bindValue(":thumbnailUrl", $imageUrl);
                $sth->bindParam(":postId", $postId);
                $sth->bindParam(":title", $title);
                $sth->execute();
            } else {
                // there is a problem with the image upload
                $sth = $dbh->prepare("
                    UPDATE post
                    SET
                        title=:title,
                        content=:content,
                        categories=:categories,
                        last_updated=NOW()
                    WHERE
                        id=:postId
                ");

                $sth->bindValue(":content", $content);
                $sth->bindValue(":categories", $catString);
                $sth->bindParam(":postId", $postId);
                $sth->bindParam(":title", $title);
                $sth->execute();
            }
        } catch(PDOException $e) {
            echo "Message: ".$e->getMessage()."<br>";
            echo "Code: ".$e->getCode()."<br>";
        }
    }
}

?>