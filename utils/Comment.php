<?php

include_once "../utils/Comment.php";
include_once "../utils/Message.php";
include_once "../utils/DBConnector.php";

class Comment {
	static function createForPost($content, $postId) {
		$userId = $_COOKIE["userId"];

		$dbh = DBConnector::createConnection();

		try {
			$sth = $dbh->prepare("
				INSERT INTO comment(
					user_id,
					post_id,
					content,
					date_posted
				)
				VALUES (?,?,?,NOW());
			");

			$sth->execute(array($userId, $postId, $content));

			// close db connection
			$dbh = null;

			Message::success("Comment Posted Successfully");

			header("location: /posts/post-detail.php?id=$postId", 201);
		} catch(PDOException $e) {
			echo "Message: ".$e->getMessage()."<br>";
			echo "Code: ".$e->getCode()."<br>";
		}
	}

	static function createForComment($content, $commentId, $postId) {
		$userId = $_COOKIE["userId"];

		$dbh = DBConnector::createConnection();

		try {
			$sth = $dbh->prepare("
				INSERT INTO comment(
					user_id,
					comment_id,
					content,
					date_posted
				)
				VALUES (?,?,?,NOW());
			");

			$sth->execute(array($userId, $commentId, $content));

			// close db connection
			$dbh = null;

			Message::success("Comment Posted Successfully");

			header("location: /posts/post-detail.php?id=$postId", 201);
		} catch(PDOException $e) {
			// close db connection
			$dbh = null;
			echo "Message: ".$e->getMessage()."<br>";
			echo "Code: ".$e->getCode()."<br>";
		}
	}

	static function getReplies($commentId) {
		include_once "User.php";

		$dbh = DBConnector::createConnection();

		try {
			$sth = $dbh->prepare("
				SELECT
					user_id,
					content,
					date_posted,
					id
				FROM
					comment
				WHERE
					comment_id=?
				ORDER BY
					date_posted DESC
			");

			$sth->execute(array($commentId));
			$rows = $sth->fetchAll(PDO::FETCH_OBJ);

			// close db connection
			$dbh = null;

			// make array where the replies will be stored
			$replies = array();

			foreach($rows as $reply) {
				$userId = $reply->user_id;
				$content = $reply->content;
				$datePosted = $reply->date_posted;
				$id = $reply->id;

				$formattedDate = DateTime::createFromFormat("Y-m-d H:i:s", $datePosted);

				// use the new formatted date for readability
				$datePosted = $formattedDate->format("d.m.Y");

				$username = User::getDetails($userId)["userName"];

				// remove the "Creating default object from empty value" error
				$replies[$id] = new stdClass();

				// set the object's attributes
				$replies[$id]->userName = $username;
				$replies[$id]->content = $content;
				$replies[$id]->datePosted = $datePosted;
				$replies[$id]->id = $id;
			}
			if(!empty($replies)) {
				return $replies;
			} else {
				return null;
			}

		} catch(PDOException $e) {
			// close db connection
			$dbh = null;
			echo "Message: ".$e->getMessage()."<br>";
			echo "Code: ".$e->getCode()."<br>";
		}
	}

	static function isOwner($commentId, $loggedInUserId) {
        $dbh = DBConnector::createConnection();

        try {
            // fetch the user id of post from db
            $sth = $dbh->prepare("
                SELECT
                    user_id
                FROM
                    comment
                WHERE
                    id=:commentId
            ");

            $sth->bindParam(":commentId", $commentId, PDO::PARAM_INT);
            $sth->execute();

            $row = $sth->fetch(PDO::FETCH_OBJ);

            $userId = $row->user_id;

            // close db connection
			$dbh = null;

            // check that the current user id matches that one of the post
            if($userId == $loggedInUserId) {
                return true;
            } else {
                return false;
            }
        } catch(PDOException $e) {
            echo "Message: ".$e->getMessage()."<br>";
            echo "Code: ".$e->getCode()."<br>";
        }
    }

	static function update($commentId, $content) {
		$dbh = DBConnector::createConnection();

		try {
			$sth = $dbh->prepare("
				UPDATE comment
				SET
					content=:content,
					last_updated=NOW()
				WHERE
					id=:commentId
			");

			$sth->bindParam(":commentId", $commentId, PDO::PARAM_INT);
			$sth->bindValue(":content", $content);

			$sth->execute();

			Message::success("The Comment Was Updated Successfully");

			$dbh = null;

		} catch(PDOException $e) {
			echo "Message: ".$e->getMessage()."<br>";
			echo "Code: ".$e->getCode()."<br>";
		}
	}

	static function delete($commentId) {
		$dbh = DBConnector::createConnection();

		try {
			$sth = $dbh->prepare("
				DELETE FROM comment
				WHERE
					id=:commentId
			");

			$sth->bindParam(":commentId", $commentId, PDO::PARAM_INT);
			$sth->execute();

			// close db connection
			$dbh = null;
		} catch(PDOException $e) {
			echo "Message: ".$e->getMessage()."<br>";
			echo "Code: ".$e->getCode()."<br>";
		}
	}
}

?>