<?php

include_once "DBConnector.php";
include_once "Message.php";
include_once "ParamChecker.php";

class User {
    static $paramsValid;

    static function addSuperUser($userId) {
        $dbh = DBConnector::createConnection();

        try {
            $sth = $dbh->prepare("
                UPDATE
                    user
                SET
                    is_admin = 1
                WHERE
                    id = :userId
            ");

            $sth->bindParam(":userId", $userId, PDO::PARAM_INT);

            $sth->execute();

            // close db connection
            $dbh = null;

            Message::success("The User Was Successfully Made A Superuser");

            header("location: /admin/user-detail.php?id=$userId");

        } catch(PDOException $e) {
            // close db connection
            $dbh = null;
            
            echo "Message: ".$e->getMessage()."<br>";
            echo "Code: ".$e->getCode()."<br>";
        }
    }

    static function checkParams($params, $message) {
        // check for empty parameters
        self::$paramsValid = ParamChecker::paramsValid($params);

        // there is an error in the parameters
        if(!self::$paramsValid) {
            Message::error($message);
            header("location: /users/sign_up.php", 400);
        }
    }

    static function create($params) {
        // check params
        self::checkParams($params, "All fields are required");

        // if params are ok proceed to create user
        $username = $params["username"];
        $firstName = $params["first-name"];
        $lastName = $params["last-name"];
        $email = $params["email"];

        // the date is already in YYYY-MM-DD format that is similar to that of mysql
        $DOB = $params["date-of-birth"];
        $password = $params["password"];

        // check for the length of the password
        if(strlen($password) < 8) {
            Message::error("The Password Should Have At Least Eight Characters");
            header("location: /users/sign_up.php", 400);
        }

        // check whether the email is correct
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            Message::error("The Email Has An Invalid Format");
            header("location: /users/sign_up.php", 400);
        }

        // everything is okay, so proceed to create user
        $conn = DBConnector::createConnection();

        try {
            // create statement handler
            $sth = $conn->prepare(
                "INSERT INTO user(
                    first_name,
                    last_name,
                    username,
                    email,
                    date_of_birth,
                    password
                )
                VALUES (?,?,?,?,?,?)
                "
            );
            // hash the password to avoid storage of raw text in database
            $encryptedPassword = password_hash($password, PASSWORD_BCRYPT);

            // check whether the user was created successfully
            $creationSucceeded = $sth->execute(
                array($firstName, $lastName, $username, $email, $DOB, $encryptedPassword)
            );

            // close the db connection
            $conn = null;

            // give user response upon success or failure
            if($creationSucceeded) {
                Message::success("User Account Created Successfully<br>Enter Details To Login");
                header("location: /users/login.php", 201);
            } else {
                Message::error("Sorry, an error occured during account creation,<br>
                try again and contact customer service if the problem persists");
                header("location: /users/login.php", 201);
            }
        } catch(PDOException $e) {
            echo "Message: ".$e->getMessage()."<br>";
            echo "Code: ".$e->getCode()."<br>";
        }
    }

    static function createFeedback() {
        /**
        *
        * check that the post parameters are all okay
        * if okay, save the feedback to the database
        * if not okay, send a response to the user telling them where the error is
        *
        **/
        $errorMessage = "";

        foreach($_POST as $postParam) {
            if(empty($postParam)) {
                $errorMessage .= $postParam.",";
            }
        }

        if(!empty($errorMessage)) {
            // remove trailing comma for ease of reading
            $errorMessage.trim(",");
            $errorMessage .= " cannot be empty";
            Message::error($errorMessage);
            header("location: /users/user-feedback.php");
        }

        $name = $_POST["name"];
        $email = $_POST["email"];
        $title = $_POST["title"];
        $content = $_POST["content"];
        $type = $_POST["type"];

        $dbh = DBConnector::createConnection();

        try {
            $sth = $dbh->prepare("
                INSERT INTO
                    user_feedback(
                        name,
                        email,
                        title,
                        content,
                        type,
                        date_posted
                    )
                VALUES (?,?,?,?,?,NOW())
            ");

            $sth->execute(array($name, $email, $title, $content, $type));

            Message::success("Thank You For Sending Us The Feedback");
            header("location: /", 200);

        } catch(PDOException $e) {
            echo "Message: ".$e->getMessage()."<br>";
            echo "Code: ".$e->getCode()."<br>";
        }

    }

    static function delete($userId, $sentByAdmin=false) {
        // check if the user id is a valid one
        if(empty($userId)) {
            return [false, "The User Id Cannot Be Empty"];
        }

        // check if the user exists
        $dbh = DBConnector::createConnection();

        try {
            // prepare the statement handler
            $sth = $dbh->prepare("SELECT username FROM user WHERE id=?");

            // check if the user exists
            $sth->execute(array($userId));

            $user = $sth->fetch(PDO::FETCH_OBJ);

            if(empty($user)) {
                return [false, "That User Does Not Exist"];
            }

            // set the account activity to false
            $sth2 = $dbh->prepare(
                "UPDATE user
                SET
                    is_active=:status
                WHERE
                    id=:userId
                "
            );

            $notActive = 0;

            // deactivate the account
            $sth2->bindParam(":status", $notActive, PDO::PARAM_INT);
            $sth2->bindParam(":userId", $userId, PDO::PARAM_INT);

            $sth2->execute();

            // check if the is_active status changed
            $sth3 = $dbh->prepare("
                SELECT
                    is_active
                FROM
                    user
                WHERE
                    id=?
            ");

            // check whether account status has been set to not active
            $sth3->execute(array($userId));
            $row = $sth3->fetch(PDO::FETCH_OBJ);
            $isActive = $row->is_active;

            if(!$isActive) {
                $userDeleted = true;
            } else {
                $userDeleted = false;
            }

            // check whether user was deleted
            if($userDeleted) {
                // check whether an admin was deleting the user
                if($sentByAdmin) {
                    return [true, "Account Deleted Successfully"];
                }

                // log the user out
                if(!isset($_SESSION)) {
                    session_start();
                }
                $_SESSION["isAuthenticated"] = false;

                return [true, "Account Deleted Successfully"];
            } else {
                return [false, "Error Occured In Deleting Account"];
            }

            // close the connection
            $dbh = null;
        } catch(PDOException $e) {
            $dbh = null;

            echo "Message: ".$e->getMessage()."<br>";
            echo "Code: ".$e->getCode()."<br>";
            die();
        }
    }

    static function feedBackDetails($feedBackId) {
        $dbh = DBConnector::createConnection();

        try {
            $sth = $dbh->prepare("
                SELECT
                    name,
                    email,
                    title,
                    viewed_by,
                    content,
                    date_posted,
                    date_viewed,
                    type
                FROM
                    user_feedback
                WHERE
                    id=:feedBackId
            ");

            $sth->bindParam(":feedBackId", $feedBackId, PDO::PARAM_INT);
            $sth->execute();

            // close db connection
            $dbh = null;

            $feedback = $sth->fetch(PDO::FETCH_OBJ);

            return $feedback;
        } catch(PDOException $e) {
            // close db connection
            $dbh = null;

            echo "Message: ".$e->getMessage()."<br>";
        }
    }

    static function getDetails($userId) {
        // check if the user exists
        $dbh = DBConnector::createConnection();

        try {
            // prepare the statement handler
            $sth = $dbh->prepare("SELECT username, first_name, last_name, email, date_of_birth, profile_pic_url, bio, phone_number FROM user WHERE id=?");

            $sth->execute(array($userId));

            $row = $sth->fetch(PDO::FETCH_OBJ);

            if(empty($row)) {
                return null;
            }

            $userName = $row->username;
            $firstName = $row->first_name;
            $lastName = $row->last_name;
            $email = $row->email;
            $dateOfBirth = $row->date_of_birth;
            $imageUrl = $row->profile_pic_url;
            $bio = $row->bio;
            $phone = $row->phone_number;

            // close db connection
            $dbh = null;

            return array("firstName"=>$firstName, "lastName"=>$lastName, "userName"=>$userName, "email"=>$email, "dateOfBirth"=>$dateOfBirth, "imageUrl"=>$imageUrl, "bio"=>$phone, "phoneNumber"=>$phone);

        } catch(PDOException $e) {
            echo "Message: ".$e->getMessage()."<br>";
            echo "Code: ".$e->getCode()."<br>";
        }
    }

    static function getFeedback($page) {
        $dbh = DBConnector::createConnection();

        // get the offset
        if($page == 1) {
            $offSet = 0;
        } else {
            $offSet = ceil($page * 10 - 10);
        }

        try {
            $sth = $dbh->prepare("
                SELECT
                    id,
                    type,
                    email,
                    viewed
                FROM
                    user_feedback
                LIMIT :offSet,10
            ");

            $sth->bindParam(":offSet", $offSet, PDO::PARAM_INT);

            $sth->execute();

            $items = $sth->fetchAll(PDO::FETCH_OBJ);

            return $items;

        } catch(PDOException $e) {
            echo "Message: ".$e->getMessage()."<br>";
            echo "Code: ".$e->getCode()."<br>";
        }
    }

    static function isAdmin($userId) {
        $dbh = DBConnector::createConnection();

        try {
            $sth = $dbh->prepare("
                SELECT
                    is_admin
                FROM
                    user
                WHERE
                    id=:userId
            ");

            $sth->bindParam(":userId", $userId, PDO::PARAM_INT);

            $sth->execute();

            $row = $sth->fetch(PDO::FETCH_OBJ);

            // close database connection
            $dbh = null;

            if($row->is_admin == 1) {
                $adminStatus = true;
            } else {
                $adminStatus = false;
            }

            return $adminStatus;
        } catch(PDOException $e) {
            // close database connection
            $dbh = null;

            echo "Message: ".$e->getMessage()."<br>";
            echo "Code: ".$e->getCode()."<br>";
        }
    }

    static function login($username, $password) {
        include_once "Message.php";

        if(!isset($_SESSION)) {
            session_start();
        }

        echo "username-$username, password-$password\n";

        // check whether the items are empty
        $params = array($username, $password);

        self::checkParams($params, "Both The Username And Password Are Required");

        // create connection to the database
        $dbh = DBConnector::createConnection();

        try {
            // prepare the statement handler
            $sth = $dbh->prepare("
                SELECT
                    id,
                    username,
                    password,
                    is_active
                FROM
                    user
                WHERE
                    username=?
            ");

            // run the query
            $sth->execute(array($username));

            $row = $sth->fetch(PDO::FETCH_OBJ);

            // close the connection
            $dbh = null;

            // check if any user was returned
            if(empty($row)) {
                Message::error("Wrong Username Or Password");
                header("location: /users/login.php", 400);
            } else {
                // check if the user account was deleted
                $isActive = $row->is_active;

                // the account was previously deleted hence the user cannot login
                if(!$isActive) {
                    Message::error("Wrong Username Or Password");
                    header("location: /users/login.php", 400);
                    // prevent proceeding to login(for some weird bug which i havent yet found)
                    die();
                }
                echo "Is Active";;

                // get the hash for verification purposes
                $hash = $row->password;

                // verify the password using password_verify
                $passwordMatches = password_verify($password, $hash);

                // fetch both the id and username
                $userId = $row->id;
                $username = $row->username;

                if($passwordMatches) {
                    // indicate that the user is authenticated and return the user id
                    $_SESSION["isAuthenticated"] = true;

                    Message::success("Successfully logged in as $username");

                    setcookie("userId", $userId, time()+3600, "/");
                    $_SESSION["userId"] = $userId;
                } else {
                    // echo "password doesn't match";die();
                    // issue a warning that the password is wrong
                    Message::error("You have entered a wrong password, try again");
                    if($_SESSION["adminLogin"]) {
                        if(!empty($_SESSION["next"])) {
                            $next = $_SESSION["next"];
                            if(!empty($_SESSION["id"])) {
                                $id = $_SESSION["id"];
                                header("location: /admin/login.php?next=$next&id=$id", 200);
                                die();
                            }
                            unset($_SESSION["next"]);
                            header("location: /admin/login.php?next=$next", 200);
                            die();
                        } else {
                            header("location: /admin/login.php", 200);
                            die();
                        }
                    }
                    header("location: /users/login.php?next=", 200);
                }

            }
        } catch(PDOException $e) {
            echo "Message: ".$e->getMessage()."<br>";
        }
    }

    static function logout() {
        session_start();

        // unset the session cookie
        setcookie("userId", "", time() - 1, "/");
        $_SESSION["isAuthenticated"] = false;

        // send them the notification that they logged out successfully and redirect to home page
        Message::info("User Logged Out Successfully");

        $next = empty($_GET["next"]) ? "" : $_GET["next"];

        if($next == "admin-login") {
            header("location: /admin/login.php");
        }

        header("location: /", 200);
    }

    static function removeSuperUser($userId) {
        $dbh = DBConnector::createConnection();

        try {
            $sth = $dbh->prepare("
                UPDATE
                    user
                SET
                    is_admin = 0
                WHERE
                    id = :userId
            ");

            $sth->bindParam(":userId", $userId, PDO::PARAM_INT);

            $sth->execute();

            // close db connection
            $dbh = null;

            Message::success("The Superuser Status Was Removed Successfully");

            header("location: /admin/user-detail.php?id=$userId");

        } catch(PDOException $e) {
            // close db connection
            $dbh = null;

            echo "Message: ".$e->getMessage()."<br>";
            echo "Code: ".$e->getCode()."<br>";
        }
    }

    static function update($params) {
        // fetch the required parameters
        $firstName = $params["first-name"];
        $lastName = $params["last-name"];
        $userName = $params["username"];
        $email = $params["email"];
        $DOB = $params["date-of-birth"];
        $bio = $params["bio"];
        $phone = $params["phone"];

        // get the user id
        $userId = $_COOKIE["userId"];

        try {
            // create connection
            $dbh = DBConnector::createConnection();

            $sth = $dbh->prepare(
                "UPDATE user
                SET
                    first_name=?,
                    last_name=?,
                    username=?,
                    email=?,
                    date_of_birth=?,
                    bio=?,
                    phone_number=?
                WHERE
                    id=?
            ");

            $updatedSuccessfully = $sth->execute(
                array($firstName, $lastName, $userName, $email, $DOB, $bio, $phone, $userId)
            );

            // close db connection
            $dbh = null;

            // return a response based on whether the account was updated successfully or update failed
            if($updatedSuccessfully) {
                Message::success("The user profile was updated successfully");
                header("location: /", 204);
            } else {
                Message::error("Sorry, an error occured while updating the profile, try again");
                header("location: /users/update_profile.php", 204);
            }
            
        } catch(PDOException $e) {
            echo "Message: ".$e->getMessage()."<br>";
            echo "Code: ".$e->getCode()."<br>";
        }
    }

    static function userList($page) {
        $dbh = DBConnector::createConnection();

        // get the offset
        if($page == 1) {
            $offSet = 0;
        } else {
            $offSet = ceil($page * 10 - 10);
        }

        try {
            $sth = $dbh->prepare("
                SELECT
                    username,
                    email,
                    id,
                    is_admin,
                    is_active
                FROM
                    user
                LIMIT :offSet,10
            ");

            $sth->bindParam(":offSet", $offSet, PDO::PARAM_INT);

            $sth->execute();

            $items = $sth->fetchAll(PDO::FETCH_OBJ);

            return $items;

        } catch(PDOException $e) {
            echo "Message: ".$e->getMessage()."<br>";
            echo "Code: ".$e->getCode()."<br>";
        }
    }

    static function viewFeedback($feedBackId, $userId) {
        /**
        *
        * change the status of the feedback to viewed and indicate the user that viewed it
        *
        **/
        $dbh = DBConnector::createConnection();

        try {
            // check whether post has already been viewed
            $sth2 = $dbh->prepare("
                SELECT
                    viewed
                FROM
                    user_feedback
                WHERE
                    id=?
            ");

            $sth2->execute(array($feedBackId));

            $row = $sth2->fetch(PDO::FETCH_OBJ);

            $viewed = $row->viewed;

            if($viewed == 1) {
                $viewed == true;
            } else {
                $viewed == false;
            }

            if(!$viewed) {
                $sth = $dbh->prepare("
                    UPDATE
                        user_feedback
                    SET
                        viewed=1,
                        viewed_by=?,
                        date_viewed=NOW()
                    WHERE
                        id=?
                ");
                $sth->execute(array($userId, $feedBackId));
            }

        } catch(PDOException $e) {
            echo "Message: ".$e->getMessage()."<br>";
            echo "Code: ".$e->getCode()."<br>";
        }
    }
}

?>