<?php

include_once "DBConnector.php";

class Subscriber {
    static function delete($subId) {
        $dbh = DBConnector::createConnection();

        try {
            $sth = $dbh->prepare("
                DELETE FROM subscription
                WHERE id=?
            ");

            $sth->execute(array($subId));

            // close db
            $dbh = null;
        } catch(PDOException $e) {
            echo "Message: ".$e->getMessage()."<br";
        }
    }

    static function deleteDuplicates() {
        $dbh = DBConnector::createConnection();

        try {
            $sth = $dbh->prepare("
                DELETE t1
                FROM
                    subscription t1 INNER JOIN subscription t2
                WHERE
                    t1.id < t2.id AND t1.email = t2.email
            ");


            $sth->execute();

            // close db
            $dbh = null;

        } catch(PDOException $e) {
            echo "Message: ".$e->getMessage()."<br";
        }
    }

    static function getSubscribersList($page) {
        // get the offset
        if($page == 1) {
            $offSet = 0;
        } else {
            $offSet = ceil($page * 10 - 10);
        }

        $dbh = DBConnector::createConnection();

        try {
            $sth = $dbh->prepare("
                SELECT *
                FROM subscription
                LIMIT :offSet,10
            ");

            $sth->bindParam(":offSet", $offSet, PDO::PARAM_INT);

            $sth->execute();
            $row = $sth->fetchAll(PDO::FETCH_OBJ);

            // close db
            $dbh = null;

            return $row;

        } catch(PDOException $e) {
            echo "Message: ".$e->getMessage()."<br";
        }
    }

    static function getSubscriberEmails() {
        $dbh = DBConnector::createConnection();

        try {
            $sth = $dbh->prepare("
                SELECT email
                FROM subscription
            ");

            $sth->execute();
            $row = $sth->fetchAll(PDO::FETCH_OBJ);

            // close db
            $dbh = null;

            return $row;

        } catch(PDOException $e) {
            echo "Message: ".$e->getMessage()."<br";
        }
    }
}

?>