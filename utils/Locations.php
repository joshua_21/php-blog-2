<?php

include_once "DBConnector.php";
include_once "Message.php";

class Location {
    static function createHO($city, $openHours, $street) {
        if(empty(trim($city)) || empty(trim($openHours)) || empty(trim($street))) {
            Message::error("All Fields Are Required");
            header("location: /admin/locations.php", 400);
        }

        $dbh = DBConnector::createConnection();

        try {
            $sth = $dbh->prepare("
                INSERT INTO
                    locations(city, open_hours, street, is_head_office)
                VALUES(?,?,?,?)
            ");
            $ho = "Y";
            $sth->execute(array($city, $openHours, $street, $ho));

            // close db connection
            $dbh = null;

            Message::success("location created successfully");
            header("location: /admin/locations.php", 201);

        } catch(PDOException $e) {
            // close db connection
            $dbh = null;

            echo "Message: ".$e->getMessage()."<br";
        }
    }    

    static function createLocation($city, $openHours, $street) {
        if(empty(trim($city)) || empty(trim($openHours)) || empty(trim($street))) {
            Message::error("All Fields Are Required");
            header("location: /admin/locations.php", 400);
        }

        $dbh = DBConnector::createConnection();

        try {
            $sth = $dbh->prepare("
                INSERT INTO
                    locations(city, open_hours, street)
                VALUES(?,?,?)
            ");

            $sth->execute(array($city, $openHours, $street));

            // close db connection
            $dbh = null;

            Message::success("location created successfully");
            header("location: /admin/locations.php", 201);

        } catch(PDOException $e) {
            // close db connection
            $dbh = null;

            echo "Message: ".$e->getMessage()."<br";
        }
    }

    static function getLocations() {
        $dbh = DBConnector::createConnection();
        try {
            $sth = $dbh->prepare("
                SELECT
                    *
                FROM
                    locations
                ORDER BY
                    is_head_office DESC
            ");

            $sth->execute();
            $rows = $sth->fetchAll(PDO::FETCH_OBJ);
            // close db connection
            $dbh = null;

            return $rows;
        } catch(PDOException $e) {
            // close db connection
            $dbh = null;

            echo "Message: ".$e->getMessage()."<br>";
        }
    }

    static function HOPresent() {
        $dbh = DBConnector::createConnection();
        try {
            $sth = $dbh->prepare("
                SELECT *
                FROM
                    locations
                WHERE
                is_head_office=?
                    
            ");
            $isHeadOffice = "Y";
            $sth->execute(array($isHeadOffice));
            $row = $sth->fetch(PDO::FETCH_OBJ);

            // close db connection
            $dbh = null;

            if(!empty($row)) {
                return true;
            } else {
                return false;
            }

        } catch(PDOException $e) {
            // close db connection
            $dbh = null;

            echo "Message: ".$e->getMessage()."<br>";
        } 
    }

    static function updateLocation($locationId, $city, $openHours, $street) {
        $dbh = DBConnector::createConnection();
        try {
            $sth = $dbh->prepare("
                UPDATE locations
                SET
                    city=?,
                    open_hours=?,
                    street=?
                WHERE
                    id=?
            ");

            $sth->execute(array($city, $openHours, $street, $locationId));

            // close db connection
            $dbh = null;

        } catch(PDOException $e) {
            // close db connection
            $dbh = null;

            echo "Message: ".$e->getMessage()."<br>";
        }
    }

    static function deleteLocation($locationId) {
        $dbh = DBConnector::createConnection();
        try {
            $sth = $dbh->prepare("
                DELETE FROM locations
                WHERE
                    id=?
            ");

            $sth->execute(array($locationId));
            // close db connection
            $dbh = null;

        } catch(PDOException $e) {
            // close db connection
            $dbh = null;

            echo "Message: ".$e->getMessage()."<br>";
        }
    }


}

?>