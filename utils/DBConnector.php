<?php

class DBConnector {
    static $conn;

    // to close the db connection, simply set the database handler to null

    static function createConnection() {
        include "consts.php";

        // data source name
        $dsn = "mysql:host=".$HOST.";dbname=".$DB_NAME;

        // create new db connection
        self::$conn = new PDO($dsn, $USERNAME, $PASSWORD);

        // set attributes for error handling
        self::$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        // return the connection
        return self::$conn;
    }
}

?>