<?php

include_once "DBConnector.php";
include_once "Message.php";
include_once "User.php";

class Bio {
    static function bioExists() {
        $dbh = DBConnector::createConnection();

        // the restriction that ensures only one row can exist in the database
        $uniqueRow = "unique";

        try {
            // check if the bio already exists
            $sthBioExists = $dbh->prepare("
                SELECT *
                FROM
                    bio
                WHERE
                    row=?
            ");
            $sthBioExists->execute(array($uniqueRow));
            $row = $sthBioExists->fetch(PDO::FETCH_OBJ);

            // close db connection
            $dbh = null;

            if(empty($row)) {
                // the bio is empty
                return false;
            } else {
                // the bio is not empty
                return true;
            }
        } catch(PDOException $e) {
            // close db connection
            $dbh = null;

            echo "Message: ".$e->getMessage()."<br>";
        }
    }

    private static function formatDate($sqlDate) {
        $date = DateTime::createFromFormat("Y-m-d H:i:s", $sqlDate);
        $newDate = $date->format("d/m/Y");
        return $newDate;
    }

    static function getBio() {
        $dbh = DBConnector::createConnection();

        // the restriction that ensures only one row can exist in the database
        $row = "unique";

        try {
            // check if the bio already exists
            $sthBioExists = $dbh->prepare("
                SELECT *
                FROM
                    bio
                WHERE
                    row=?
            ");
            $sthBioExists->execute(array($row));
            $row = $sthBioExists->fetch(PDO::FETCH_OBJ);

            // close db connection
            $dbh = null;

            $userId = $row->edited_by;
            $returnedDate = $row->last_updated;
            $returedContent = $row->content;

            $userName = User::getDetails($userId)["userName"];

            // initialize object to return
            $bioObj = new stdClass();

            $bioObj->updatedBy = $userName;
            $bioObj->lastUpdated = self::formatDate($returnedDate);
            $bioObj->content = $returedContent;

            return $bioObj;

        } catch(PDOException $e) {
            // close db connection
            $dbh = null;

            echo "Message: ".$e->getMessage()."<br>";
        }
    }

    static function updateBio($content, $userId) {
        $dbh = DBConnector::createConnection();

        // the restriction that ensures only one row can exist in the database
        $uniqueRow = 'unique';

        try {
            // check if the bio already exists
            $sthBioExists = $dbh->prepare("
                SELECT *
                FROM
                    bio
                WHERE
                    row=?
            ");
            $sthBioExists->execute(array($uniqueRow));
            $row = $sthBioExists->fetch(PDO::FETCH_OBJ);

            // the bio is empty
            if(empty($row)) {
                // echo "Row Is EMpty";die();
                $sthCreateBio = $dbh->prepare("
                    INSERT INTO
                        bio(
                            edited_by,
                            row,
                            content,
                            last_updated
                        )
                    VALUES(?,?,?,NOW())
                ");
                $sthCreateBio->execute(array($userId, $uniqueRow, $content));
            } else {
                $sthUpdateBio = $dbh->prepare("
                    UPDATE bio
                    SET
                       edited_by=:userId,
                       content=:content
                    WHERE
                        row=:uniqueRow
                ");

                $sthUpdateBio->bindParam(":userId", $userId, PDO::PARAM_INT);
                $sthUpdateBio->bindValue(":content",$content);
                $sthUpdateBio->bindValue(":uniqueRow", $uniqueRow);
                $sthUpdateBio->execute();
            }

            // close db connection
            $dbh = null;

            Message::success("The bio was updated successfully");
            header("location: /admin/bio.php");

        } catch(PDOException $e) {
            // close db connection
            $dbh = null;

            echo "Message: ".$e->getMessage()."<br>";
        }
    }
}


?>