<?php

class ParamChecker {
    function paramsValid($params) {
        // ***the $params variable should be an array

        // check for empty parameters
        foreach($params as $param) {
            // item consists only of whitespace
            if(gettype($param) == "string" && trim($param) == "") {
                return false;
            }
            
            // check other data types
            if (!isset($param) || empty($param)) {
                return false;
            }
        }

        // none of the parameters is empty
        return true;
    }
}

?>