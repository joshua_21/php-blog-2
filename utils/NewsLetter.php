<?php

class NewsLetter {
    static function save($title, $content, $authorId) {
        $dbh = DBConnector::createConnection();

        try {
            $sth = $dbh->prepare("
                INSERT INTO
                    newsletter(title, content, author_id, date_sent)
                VALUES(?,?,?,NOW())
            ");

            $sth->execute(array($title, $content, $authorId));

            // close db
            $dbh = null;

        } catch(PDOException $e) {
            // close db
            $dbh = null;

            echo "Message: ".$e->getMessage()."<br";
        }
    }

    static function newsLetterList($page) {
        // get the offset
        if($page == 1) {
            $offSet = 0;
        } else {
            $offSet = ceil($page * 10 - 10);
        }

        $dbh = DBConnector::createConnection();

        try {
            $sth = $dbh->prepare("
                SELECT
                    title, content, date_sent, username
                FROM
                    newsletter INNER JOIN user
                WHERE
                    newsletter.author_id = user.id
                LIMIT :offSet,10
            ");

            $sth->bindParam(":offSet", $offSet, PDO::PARAM_INT);

            $sth->execute();
            $row = $sth->fetchAll(PDO::FETCH_OBJ);

            // close db
            $dbh = null;

            return $row;

        } catch(PDOException $e) {
            // close db
            $dbh = null;

            echo "Message: ".$e->getMessage()."<br";
        }
    }
}

?>