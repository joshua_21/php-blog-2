<?php

include_once "DBConnector.php";

class Contact {
    static function getContacts() {
        /**
         * get the contacts from the database
         */

        $dbh = DBConnector::createConnection();

        try {
            $sth = $dbh->prepare("
                SELECT
                    phone,
                    phone_2,
                    email,
                    mail
                FROM
                    contacts
                WHERE
                    `column`=?
            ");
            $column = "only";
            $sth->execute(array($column));
            $row = $sth->fetch(PDO::FETCH_OBJ);

            // close db connection
            $dbh = null;

            return $row;

        } catch(PDOException $e) {
            // close db connection
            $dbh = null;

            echo "Message: ".$e->getMessage()."<br>";
        }
    }

    static function update($phone, $phone2, $email, $mail) {
        /**
         * takes in the given parameters and updates them on the database
         * if the contacts exist or creates the contacts if they don't exist
         * 
         * @param string $phone the mobile phone number
         * @param string $phone2 the landline phone number
         * @param string $email the company email address
         * @param string $mail the mail box address
         * @return none
         * 
         */
        $dbh = DBConnector::createConnection();

        try {
            // will be used to identify the unique row
            $column = "only";

            // check if the contacts exist first
            $sth2 = $dbh->prepare("
                SELECT
                    phone
                FROM
                    contacts
                WHERE
                    `column`=?
            ");
            $sth2->execute(array($column));
            $row = $sth2->fetch(PDO::FETCH_OBJ);

            if(empty($row)) {
                // the contacts have not yet been created
                $sth3 = $dbh->prepare("
                    INSERT INTO
                        contacts(
                            phone,
                            phone_2,
                            email,
                            mail,
                            `column`
                        )
                    VALUES(?,?,?,?,?)
                ");

                $sth3->execute(array($phone, $phone2, $email, $mail, $column));
            } else {
                // the contacts have been created and need only to only be updated
                $sth = $dbh->prepare("
                    UPDATE contacts
                    SET
                        phone=?,
                        phone_2=?,
                        email=?,
                        mail=?
                    WHERE
                        `column`=?
                ");
                $sth->execute(array($phone, $phone2, $email, $mail, $column));
            }
            
            // close db connection
            $dbh = null;

        } catch(PDOException $e) {
            // close db connection
            $dbh = null;

            echo "Message: ".$e->getMessage()."<br>";
        }
    }
}

?>