<?php

include_once "utils/Contacts.php";
include_once "utils/Locations.php";

$contacts = Contact::getContacts();

$phone = $contacts->phone;
$phone2 = $contacts->phone_2;
$email = $contacts->email;
$mail = $contacts->mail;

// locations
$locations = Location::getLocations();

$headOfficePresent = Location::HOPresent();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php include "templates/css.html"; ?>
    <title>Contacts Page</title>
</head>
<body>
<?php include_once "templates/header.php"; ?>
<!-- display contacts -->
<div class="container mt-5 mb-5">
    <h2 class="mt-2">
        <u>Contacts</u>
    </h2>
    <p>
        Phone: <?php echo $phone; ?>
    </p>
    <p>
        Land Line: <?php echo $phone2; ?>
    </p>
    <p>
        Email: 
        <a href="mailto:<?php echo $email; ?>">
            <?php echo $email; ?>
        </a>
    </p>
    <p>
        Mail: <?php echo $mail; ?>
    </p>

    <div class="mt-3">
        <h2><u>Locations</u></h2>
        <?php
        foreach($locations as $location) {
            $city = $location->city;
            $openHours = $location->open_hours;
            $id = $location->id;
            $isHO = $location->is_head_office;
            $street = $location->street;

            echo '<div class="container mt-4 rounded border py-2">';
            if($isHO == "Y") {
                // for the head office
                echo '
                <div class="px-2">
                    <h3>
                        <u>Head Office</u>
                    </h3>
                    <p>City: '.$city.'</p>
                    <p>Open Hours: '.$openHours.'</p>
                    <p>Street: '.$street.'</p>
                </div>
                ';
            } else {
                // for the rest of the offices
                echo '
                    <p>City: '.$city.'</p>
                    <p>Open Hours: '.$openHours.'</p>
                    <p>Street: '.$street.'</p>
                ';
            }
            echo '</div>';
        }
        ?>
    </div>
</div>
<?php
include "templates/footer.php";
include "templates/js.html";
?>
</body>
</html>