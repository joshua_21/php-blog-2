$(
    function ()
    {
        function getElementDisplay(elementIdOrClass) {
            let sidebarSearchDisplay = $(elementIdOrClass).css('display')

            if (sidebarSearchDisplay == 'block') {
                return 'block'
            } else {
                return 'none'
            }
        }

        // contacts form
        $('#contacts-form-toggler').on('click', function() {
            let display = getElementDisplay('#contacts-form')

            if(display == 'block') {
                $("#contacts-form").css({"display": "none"})
            } else {
                $("#contacts-form").css({"display": "block"})
            }            
        })

        // head office form
        $("#btn-toggle-ho-form").on("click", function() {
            let display = getElementDisplay('#form-create-head-office')

            if(display == 'block') {
                $("#form-create-head-office").css({"display": "none"})
            } else {
                $("#form-create-head-office").css({"display": "block"})
            }
        })

        // bio form
        $('#btn-toggle-bio-update-form').on("click", function() {
            let display = getElementDisplay('#form-update-bio')

            if(display == 'block') {
                $("#form-update-bio").css({"display": "none"})
            } else {
                $("#form-update-bio").css({"display": "block"})
            }
        })

        // toggle form for adding location
        $("#btn-add-location").on('click', function() {
            let display = getElementDisplay("#form-add-location")

            if(display == "block") {
                $("#form-add-location").css({"display":"none"})
            } else {
                $("#form-add-location").css({"display":"block"})
            }
        })

        // toggle form for updating location
        $(".btn-toggle-update-form").on('click', function(e) {
            let btnId = e.target.id
            let locationId = btnId.split("-")[3]

            let display = getElementDisplay(`#form-update-location-${locationId}`)

            if(display == "block") {
                $(`#form-update-location-${locationId}`).css({"display":"none"})
            } else {
                $(`#form-update-location-${locationId}`).css({"display":"block"})
            }
        })

        // toggle form for deleting location
        $(".btn-toggle-delete-form").on('click', function(e) {
            let btnId = e.target.id
            let locationId = btnId.split("-")[3]

            let display = getElementDisplay(`#form-delete-location-${locationId}`)

            if(display == "block") {
                $(`#form-delete-location-${locationId}`).css({"display":"none"})
            } else {
                $(`#form-delete-location-${locationId}`).css({"display":"block"})
            }
        })

        // minimise deletion form
        $(".minimise-delete-form").on("click", function(e) {
            let btnId = e.target.id
            let locationId = btnId.split("-")[1]

            let display = getElementDisplay(`#form-delete-location-${locationId}`)

            if(display == "block") {
                $(`#form-delete-location-${locationId}`).css({"display":"none"})
            } else {
                $(`#form-delete-location-${locationId}`).css({"display":"block"})
            }
        })

        // delete user popup
        $('.toggle-delete-popup').on('click', function(e) {
            let id = e.target.id;

            let userId = id.split("-")[2];

            $(`#delete-popup-${userId}`).css("display", "initial");
        })

        // remove delete user popup
        $('.close-delete-popup').on('click', function() {
            $('.user-delete-popup').css("display", "none");
        })

        // update comment box
        $('.update-comment-trigger').on('click', function(e) {
            // get the id of the comment
            let commentId = e.target.id

            // get the id of the post
            let postId = $('#post-id-holder').text()

            // get the content of the comment
            let commentContent = $(`#content-${commentId}`).text()

            // hide everything inside the comment display box
            $(`#comment-box-${commentId} *`).css({'display': 'none'})

            // check that the form does not yet exist
            let commentUpdateForm = $(`#update-comment-${commentId}`)

            // make the comment box if it doesn't exist
            if(commentUpdateForm.css("display") == undefined) {
                // show the comment form
                $(`#comment-box-${commentId}`).append(`
                    <form method="POST" action="/comments/update-comment.php" id="update-comment-${commentId}">
                        <textarea name="comment-content">${commentContent.trim()}</textarea>
                        <br>
                        <input type="hidden" name="comment-id" value="${commentId}">
                        <input type="hidden" name="post-id" value="${postId}">
                        <input type="submit" value="Update">
                    </form>
                `)
            } else {
                // do nothing if the comment box is displayed
            }
        })
        
        // sidebar activity
        $("#sidebar-toggler").on('click', function() {
            $('#sidebar').css({
                'display': 'initial'
            })
        })

        $("#sidebar-close-icon").on("click", function(){
            $("#sidebar").css({
                "display": "none"
            })
        })

        // indicate the current file when editing profile
        $("#profile-picture").change(function(e) {
            $("#profile-pic-name").text("");
        })

        // delete popup
        $('#delete-btn').on('click', function() {
            $('.delete-popup').css({
                'display': 'block'
            })
        })

        $('#cancel-delete-btn').on('click', function() {
            $('.delete-popup').css({
                'display': 'none'
            })
        })

        // comment-btn comment-box
        $('#comment-btn').on('click', function() {
            let commentBoxDisplay = getElementDisplay('#comment-box')

            if (commentBoxDisplay == 'none') {
                $('#comment-box').css({
                    'display': 'block'
                })
            } else {
                $('#comment-box').css({
                    'display': 'none'
                })
            }
        })

        // reply to a comment
        $(".comment-reply-trigger").on('click', function(e) {
            let commentId = e.target.id

            // show the comment reply box
            $(`#reply-comment-${commentId}`).css({"display": "initial"})
        })

        // confirm that passwords are equal
        $("#sign-up-form").submit(function(e) {
            let password1 = $("#password").val();
            let password2 = $("#password2").val();

            if(password1 != password2) {
                e.preventDefault();
                $('#unmatched-passwords-alert').css({
                    'display': 'block'
                })

                // make the alert disappear so user can be alerted
                // in the case of another password mismatch
                setTimeout(function() {
                    $("#unmatched-passwords-alert").css({"display": "none"})
                }, 3000)
            }


        })
    }
)