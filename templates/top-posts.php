<?php

$ROOT_DIR = $_SERVER["DOCUMENT_ROOT"];

include_once "$ROOT_DIR/utils/Post.php";

// get the top posts from the database
$posts = Post::topPosts(3);

// obtain the posts separately
$post1 = $posts[0];
$post2 = $posts[1];
$post3 = $posts[2];

?>

<div class="container mb-4">
    <h2>
        <u>Our Featured Posts</u>
    </h2>
</div>

<div class="container">
    <!-- top post -->
    <div class="row">
        <div class="col-sm-6 col-12">
            <div class="row container-fluid">
                <div class="">
                    <a href="/posts/post-detail.php?id=<?php echo $post1->id; ?>">
                        <img class="w-100 rounded" src="<?php echo $post1->thumbnail_url; ?>" alt="top post image">
                        <span><?php echo $post1->title ?></span>
                    </a>
                </div>
                <p><?php echo substr($post1->content, 0, 500); ?>........</p>
            </div>
        </div>
        <div class="col-sm-6 col-12">
            <!-- second post -->
            <div class="col-12 mb-3">
                <div class="row">
                    <a href="/posts/post-detail.php?id=<?php echo $post2->id; ?>">
                        <img class="rounded col-sm-12 col-md-9" src="<?php echo $post2->thumbnail_url; ?>" alt="Top Post Image">
                        <span><?php echo $post2->title ?></span>
                    </a>
                </div>
            </div>
            <!-- third post -->
            <div class="col-12">
                <div class="row">
                    <a href="/posts/post-detail.php?id=<?php echo $post3->id; ?>">
                        <img class="rounded col-sm-12 col-md-9" src="<?php echo $post3->thumbnail_url; ?>" alt="Top Post Image">
                        <span><?php echo $post3->title; ?></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>