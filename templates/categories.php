<div class="container mt-5">
    <h2>
        <u>Categories:</u>
    </h2>
    <div class="row">
        <div class="col-6">
            <p>
                <a href="../posts/posts-per-category.php?cat=business">Business</a>
            </p>
            <p>
                <a href="../posts/posts-per-category.php?cat=education">Education</a>
            </p>
            <p>
                <a href="../posts/posts-per-category.php?cat=fashion">Fashion</a>
            </p>
            <p>
                <a href="../posts/posts-per-category.php?cat=food">Food</a>
            </p>
        </div>
        <div class="col-6">
            <p>
                <a href="../posts/posts-per-category.php?cat=health">Health</a>
            </p>
            <p>
                <a href="../posts/posts-per-category.php?cat=lifestyle">Lifestyle</a>
            </p>
            <p>
                <a href="../posts/posts-per-category.php?cat=politics">Politics</a>
            </p>
            <p>
                <a href="../posts/posts-per-category.php?cat=sports">Sports</a>
            </p>
        </div>
    </div>
</div>