<div class="container mt-5 border py-3 rounded border-info">
	<p>Subscribe to our weekly newsletter to receive new updates, we treat privacy with the utmost priority and all your details will be kept secure</p>
	<?php
	if(!empty($_GET["id"])) {
		$postId = $_GET["id"];
	} else {
		$postId = null;
	}
	?>
	<form method="post" class="w-md-50" action="../users/subscribe.php">
		<div class="form-group">
			<label for="name">Name</label>
			<input class="form-control" type="text" name="name" id="name">
		</div>
		<?php
		if(!empty($postId)) {
			echo '<input type="hidden" name="post-id" value="'.$postId.'">';
		} 
		?>
		<div class="form-group">
			<label for="email">Email</label>
			<input class="form-control" type="email" name="email" id="email">
		</div>
		<input type="submit" value="subscribe">
	</form>
</div>