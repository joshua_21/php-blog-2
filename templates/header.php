<!-- top navbar -->
<nav class="navbar navbar-expand-lg navbar-light bg-light px-sm-5">
  <a class="navbar-brand" href="/">Blog</a>
  <div class="ml-auto row">
  <!-- user profile image -->
  <?php
  if(!isset($_SESSION)) {
    session_start();
  }

  // set variable for checking whether the user is logged in
  if(!empty($_COOKIE["userId"]) && $_SESSION["isAuthenticated"] == true) {
    $isLoggedIn = true;
  } else {
    $isLoggedIn = false;
  }

  echo '<a href="/users/profile.php">';
  $root = $_SERVER["DOCUMENT_ROOT"];

  include_once $root."/utils/User.php";

  if($isLoggedIn) {
    // get the logged in user's image
    $loggedInUserId = $_COOKIE["userId"];
    $profilePic = User::getDetails($loggedInUserId)["imageUrl"];
    echo '<img class="hide-xs" width="50" src="'.$profilePic.'" alt="i">';
  }
  echo "</a>";
  ?>
    <form class="hide-sm form-inline my-2 my-lg-0 ml-2" action="/posts/search-results.php">
      <input class="form-control mr-sm-2" name="q" type="search" placeholder="search for post" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
    <span id="sidebar-toggler" class="hide-md ml-auto mr-2 ml-2">&#9776;</span>
  </div>
</nav>
<!-- bottom navbar -->
<nav class="mb-sm-5 hide-xs navbar navbar-expand navbar-light bg-light align-content-center">
    <ul class="navbar-nav mx-auto">
      <li class="nav-item active">
        <a class="nav-link" href="/">Home</a>
      </li>
      <li class="nav-item">
      <?php
      if($isLoggedIn) {
        echo '<a class="nav-link" href="../users/logout.php">Logout</a>';
      } else {
        echo '<a class="nav-link" href="../users/login.php">Login</a>';
      }
      ?>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../about.php">About</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../contacts.php">Contacts</a>
      </li>
      <li class="nav-item hide-sm">
        <a class="nav-link" href="/users/user-feedback.php">Feedback</a>
      </li>
      <li class="nav-item">
        <?php
        if($isLoggedIn) {
          echo '<a class="nav-link" href="../posts/new-post.php">+Add Post</a>';
        }
        ?>
      </li>
    </ul>
</nav>

<!-- sidebar -->
<div id="sidebar" class="container hide-md">
    <span class="d-flex ml-2 mt-0 p-0" id="sidebar-close-icon">&times;</span>
    <p>
        <a href="/" class="hide-sm show-xs">Home</a>
    </p>
    <?php
      if($isLoggedIn) {
        echo '<p><a class="hide-sm show-xs" href="../users/logout.php">Logout</a></p>';
      } else {
        echo '<p><a class="hide-sm show-xs" href="../users/login.php">Login</a></p>';
      }
      ?>
    <p>
        <a href="../about.php" class="hide-sm show-xs">About</a>
    </p>
    <p>
        <a href="../contacts.php" class="hide-sm show-xs">Contacts</a>
    </p>
    <p>
        <a href="/users/user-feedback.php" class="hide-sm show-xs">Feedback</a>
    </p>
    <p class="hide-sm show-xs">
        <!-- user profile image -->
        <?php
        echo '<a href="/users/profile.php">';
        $root = $_SERVER["DOCUMENT_ROOT"];

        include_once $root."/utils/User.php";

        if($isLoggedIn) {
          // get the logged in user's image
          $loggedInUserId = $_COOKIE["userId"];
          $profilePic = User::getDetails($loggedInUserId)["imageUrl"];
          echo '<img class="show-xs mb-2" width="50" src="'.$profilePic.'" alt="i">';
        }
        echo '</a>';
        ?>
    </p>
    <li class="nav-item hide-xs">
        <a class="nav-link" href="/users/user-feedback.php">Feedback</a>
      </li>
    <form class="show-sm show-xs form-inline" action="/posts/search-results.php">
        <input name="q" class="form-control mr-sm-2" type="search" placeholder="search for post" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
</div>