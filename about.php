<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php
    include_once "templates/css.html";
    session_start();
    ?>
    <title>About</title>
</head>
<body>

<?php include_once "templates/header.php"; ?>

<div class="container my-5">
    <p>
        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Est, dolor placeat
         eaque nemo quo hic modi? Minus, delectus exercitationem sapiente iusto ea 
         esse quisquam, reprehenderit unde maxime laborum dolorum voluptates!
    </p>
    <p>
        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Est, dolor placeat
         eaque nemo quo hic modi? Minus, delectus exercitationem sapiente iusto ea 
         esse quisquam, reprehenderit unde maxime laborum dolorum voluptates!
    </p>
    <p>
        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Est, dolor placeat
         eaque nemo quo hic modi? Minus, delectus exercitationem sapiente iusto ea 
         esse quisquam, reprehenderit unde maxime laborum dolorum voluptates!
    </p>
    <p>
        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Est, dolor placeat
         eaque nemo quo hic modi? Minus, delectus exercitationem sapiente iusto ea 
         esse quisquam, reprehenderit unde maxime laborum dolorum voluptates!
    </p>
    <p>
        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Est, dolor placeat
         eaque nemo quo hic modi? Minus, delectus exercitationem sapiente iusto ea 
         esse quisquam, reprehenderit unde maxime laborum dolorum voluptates!
    </p>
</div>

<?php
include_once "templates/footer.php";
include_once "templates/js.html";
?>

</body>
</html>