<?php

if(!isset($_SESSION)) {
	session_start();
}

// set variable for checking whether the user is logged in
if(!empty($_COOKIE["userId"]) && $_SESSION["isAuthenticated"] == true) {
	$isLoggedIn = true;
} else {
	$isLoggedIn = false;
}

if(!$isLoggedIn) {
	// unset the user id
	setcookie("userId", "", time()-3600);
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php include "templates/css.html"; ?>
    <title>Scammers Inc</title>
</head>
<body>

<?php

include_once "utils/Message.php";

// display alert
Message::show_message();

include_once "templates/header.php";
include_once "templates/top-posts.php";
include_once "templates/categories.php";
include_once "templates/user-subscription-form.php";
include "templates/js.html";
include "templates/footer.php";

?>

</body>
</html>