<?php
include_once "../utils/Message.php";
include_once "../utils/Comment.php";

if(!isset($_SESSION)) {
	session_start();
}

$commentContent = $_POST["comment-content"];

$postId = $_POST["post-id"];

$commentId = empty($_POST["comment-id"]) ? "" : $_POST["comment-id"];

// check whether user is authenticated
if($_SESSION["isAuthenticated"] && !empty($_COOKIE["userId"])) {
	if(empty($commentContent)) {
		Message::error("The Comment Body Cannot Be Empty");
		header("location: /posts/post-detail.php?id=$postId", 400);
	}

	// everything is ok, proceed to create comment
	if(!empty($commentId)) {
		// the comment belongs to a post
		Comment::createForComment($commentContent, $commentId, $postId);
	} elseif(!empty($postId)) {
		// the comment belongs to a comment
		Comment::createForPost($commentContent, $postId);
	} else {
		Message::error("Invalid data sent");
		header("location: /posts/post-detail.php?id=$postId", 400);
	}
} else {
	Message::info("Log In To Continue");
	header("location: /users/login.php?next=comment&id=$postId");
}

?>