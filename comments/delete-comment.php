<?php

include_once "../utils/Comment.php";
include_once "../utils/Message.php";

if(!isset($_SESSION)) {
	session_start();
}

$userId = $_COOKIE["userId"];
$isAuthenticated = $_SESSION["isAuthenticated"];

if(!empty($userId) && $_SESSION["isAuthenticated"] == true) {
	$isLoggedIn = true;
}

// get the comment id from the post parameters
$postId = $_POST["post-id"];
$commentId = $_POST["comment-id"];

// ensure user is logged in
if($isLoggedIn) {
	// the user is the owner hence proceed to delete
	if(Comment::isOwner($commentId, $userId)) {
		// delete comment
		Comment::delete($commentId);
		// return success message to user
		Message::success("The Comment Was Deleted Successfully");
		header("location: /posts/post-detail.php?id=$postId", 204);
	} else {
		echo "User Not Owner, -> id = $commentId, user = $userId";die();
		// the user is not the owner hence no permission
		header("location: /403.php", 403);
	}
} else {
	Message::info("Log In To Continue");
	header("location: /users/login.php?next=comment&id=$postId");
}

?>