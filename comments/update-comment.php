<?php

include_once "../utils/Comment.php";

// start the session
if(!isset($_SESSION)) {
	session_start();
}

$userId = $_COOKIE["userId"];

$commentId = $_POST["comment-id"];
$content = $_POST["comment-content"];
$postId = $_POST["post-id"];

// check if the user is authenticated
if(!empty($commentId) && $_SESSION["isAuthenticated"] ==  true) {
	// check whether the user is the owner of the comment
	if(Comment::isOwner($commentId, $userId)) {
		// update the comment
		Comment::update($commentId, $content);
		// create a success message
		Message::success("The Comment Was Updated Successfully");
		// redirect user back to the post page
		header("location: /posts/post-detail.php?id=$postId", 200);
	}
} else {
	header("location: /users/login.php?next=comment&id=$postId");
}

?>